from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.lib.pagesizes import A4
from reportlab.lib import colors
from reportlab.lib.colors import black
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.platypus import Paragraph, Table, TableStyle
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER
from reportlab.graphics.renderPM import PMCanvas

from matplotlib import pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.figure import Figure
from pylab import *

import datetime
import numpy as np

import Coma
import os
import tabula

class Start(QMainWindow):

    def __init__(self):        
        super(Start, self).__init__()
        uic.loadUi("Initial.ui", self)
        
        self.setWindowIcon(QIcon('0.ico'))

        self.Label_Name_Software.setText("Confirmación Metrológica")
        self.Label_Version_Software.setText("Versión: 1.0.0")
        self.Label_Actualizacion_Software.setText("") #Actualización: 2024-01-10

        font = QtGui.QFont()        
        font.setPointSize(9)
        self.label_Imag_2.setFont(font)        
        pixmap = QPixmap('iconos\logo.png')
        self.label_Imag_2.setPixmap(pixmap)

        self.IngresarBtn.clicked.connect(self.Ingresar)
        self.SalirBtn.clicked.connect(self.close)

    def Ingresar(self):
        Usuario = self.lineEdit_Usuario.text()
        Contrasena = self.lineEdit_Contrasena.text()
        Scritp = self.comboBox.currentText()

        if (Usuario == "SUPERUSUARIO" and Contrasena == "123456"):
            if Scritp == "TERMOHIGRÓMETROS":
                self.hide()
                self.ui = Ui_Main()
                self.ui.show()
            
            elif Scritp == "CRONÓMETROS":
                self.hide()
                self.ui = Ui_Main_2()
                self.ui.show()

            elif Scritp == "PESAS":
                self.hide()
                self.ui = Ui_Main_3()
                self.ui.show()
            
            elif Scritp == "MANÓMETROS":
                self.hide()
                self.ui = Ui_Main_4()
                self.ui.show()

        else:
            mesa="Error!"
            msg = QMessageBox()
            msg.setStyleSheet("""background-color: #12abc4;
                                    color: white;            
            """)
            msg.setIcon(QMessageBox.Critical)
            msg.setText('Contraseña y/o Usuario errado')
            msg.setInformativeText(mesa)
            msg.setWindowTitle("METROIL S.A.C.")
            msg.exec_()

class Ui_Main(QMainWindow):
    
    def __init__(self):        
        super(Ui_Main, self).__init__()
        uic.loadUi("Termohigrometros.ui", self)        

        self.setWindowIcon(QIcon('0.ico'))

        
        try:
            ruta_1 = open("ruta.rut",'r', encoding='utf-8')
            ruta = ruta_1.readline()
            self.ruta = ruta
            self.ruta = r'{}'.format(self.ruta)
            ruta_1.close()
        except Exception as e:
            print(e)

        unid = open(self.ruta+"\\Base\\Supervisores.til",'r',encoding='utf-8')        
        unida = unid.readlines()            
        unid.close()

        for i in range(0, len(unida)):
            valunida = unida[i].split('\n')                
            self.comboBox_2.addItem(valunida[0])
        self.comboBox_2.setCurrentIndex(1)


        self.table_T = QTableWidget(0, 7)

        self.table_T.setHorizontalHeaderLabels(['Indicación \n (°C)',
                                                'Corrección \n (°C)',
                                                'Incertidumbre \n (°C)',
                                                'E.M.P. \n ( ± °C)',
                                                '|E.M.P.| - U \n (°C)',
                                                '|E.M.P.| + U \n (°C)',
                                                'Conclusión'])
        self.table_T.horizontalHeader().setStyleSheet("::section{Background-color: #1c3048}")
        self.table_T.setSelectionBehavior(QTableWidget.SelectRows)
        self.table_T.setSelectionMode(QTableWidget.SingleSelection) 
        header = self.table_T.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.Stretch)
        self.table_T.verticalHeader().setVisible(False)
        self.table_T.setRowCount(3)

        self.gridLayout_4.addWidget(self.table_T)


        self.table_H = QTableWidget(0, 7)

        self.table_H.setHorizontalHeaderLabels(['Indicación \n (% H.R.)',
                                                'Corrección \n (% H.R.)',
                                                'Incertidumbre \n (% H.R.)',
                                                'E.M.P. \n ( ± % H.R.)',
                                                '|E.M.P.| - U \n (% H.R.)',
                                                '|E.M.P.| + U \n (% H.R.)',
                                                'Conclusión'])
        self.table_H.horizontalHeader().setStyleSheet("::section{Background-color: #1c3048}")
        self.table_H.setSelectionBehavior(QTableWidget.SelectRows)
        self.table_H.setSelectionMode(QTableWidget.SingleSelection) 
        header = self.table_H.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.Stretch)
        self.table_H.verticalHeader().setVisible(False)
        self.table_H.setRowCount(3)

        self.gridLayout_6.addWidget(self.table_H)

        self.menuBtn.clicked.connect(self.AgrandarMenuLateral)

        self.GuardarBtn.clicked.connect(self.GuardarInfo)
        self.AbrirBtn.clicked.connect(self.AbrirFile)
        self.FilaBtn.clicked.connect(self.AddFila)
        self.RefrescarBtn.clicked.connect(self.Refresh)
        self.GenerarBtn.clicked.connect(self.Generar)
        self.RetornarBtn.clicked.connect(self.Regresar)
        self.MenosBtn.clicked.connect(self.DeleteFile)
        self.informacionBtn.clicked.connect(self.Information)

    def Information(self):
        Ui_Informacion().exec_()

    def DeleteFile(self):
        Row_T = self.table_T.rowCount()
        self.table_T.setRowCount(Row_T - 1)

        Row_H = self.table_H.rowCount()
        self.table_H.setRowCount(Row_H - 1)
    
    def Regresar(self):
        self.close()
        self.ui = Start()
        self.ui.show()

    def Calculo(self, EMP, U, Corr):
        
        EMP_U_Menos = np.abs(EMP) - U
        EMP_U_Mas = np.abs(EMP) + U

        if (np.abs(Corr) <= EMP_U_Menos):
            Mensaje = "Aceptable"
        else:
            if (np.abs(Corr) > EMP_U_Mas):
                Mensaje = "No Aceptable"
            else:
                Mensaje = "Indeterminado"


        return EMP_U_Menos, EMP_U_Mas, Mensaje

    def Generar(self):
        try:
            LinesEdits = np.zeros(15, dtype = object)
            for i in range(2, 17):
                LinesEdits[i-2] = getattr(self, "lineEdit_{}".format(i)).text()
            
            ComboBoxs = np.zeros(2, dtype = object)
            for i in range(1, 3):
                ComboBoxs[i-1] = getattr(self, "comboBox_{}".format(i)).currentText()
            
            SpinBoxs = np.zeros(2)
            for i in range(1, 3):
                SpinBoxs[i-1] = getattr(self, "spinBox_{}".format(i)).value()
            
            Row_T = self.table_T.rowCount()

            Indicacion_T = np.zeros(Row_T)
            Correccion_T = np.zeros(Row_T)
            Incertidumbre_T = np.zeros(Row_T)
            EMP_T = np.zeros(Row_T)

            for i in range(0, Row_T):
                item = self.table_T.item(i, 0)
                valor = float(str(item.text()).replace(',', '.'))
                Indicacion_T[i] = valor

                item = self.table_T.item(i, 1)
                valor = float(str(item.text()).replace(',', '.'))
                Correccion_T[i] = valor

                item = self.table_T.item(i, 2)
                valor = float(str(item.text()).replace(',', '.'))
                Incertidumbre_T[i] = valor

                item = self.table_T.item(i, 3)
                valor = float(str(item.text()).replace(',', '.'))
                EMP_T[i] = valor
            
            Row_H = self.table_H.rowCount()

            Indicacion_H = np.zeros(Row_H)
            Correccion_H = np.zeros(Row_H)
            Incertidumbre_H = np.zeros(Row_H)
            EMP_H = np.zeros(Row_H)

            for i in range(0, Row_H):
                item = self.table_H.item(i, 0)
                valor = float(str(item.text()).replace(',', '.'))
                Indicacion_H[i] = valor

                item = self.table_H.item(i, 1)
                valor = float(str(item.text()).replace(',', '.'))
                Correccion_H[i] = valor

                item = self.table_H.item(i, 2)
                valor = float(str(item.text()).replace(',', '.'))
                Incertidumbre_H[i] = valor

                item = self.table_H.item(i, 3)
                valor = float(str(item.text()).replace(',', '.'))
                EMP_H[i] = valor

            #------- Aplicar Cálculo -------#

            EMP_U_Menos_T = np.zeros(Row_T)
            EMP_U_Mas_T = np.zeros(Row_T)
            Mensaje_T = np.zeros(Row_T, dtype = object)

            EMP_U_Menos_H = np.zeros(Row_H)
            EMP_U_Mas_H = np.zeros(Row_H)
            Mensaje_H = np.zeros(Row_H, dtype = object)

            for i in range(0, Row_T):
                EMP_U_Menos_T[i], EMP_U_Mas_T[i], Mensaje_T[i] = self.Calculo(EMP_T[i], Incertidumbre_T[i], Correccion_T[i])
            
            for i in range(0, Row_H):
                EMP_U_Menos_H[i], EMP_U_Mas_H[i], Mensaje_H[i] = self.Calculo(EMP_H[i], Incertidumbre_H[i], Correccion_H[i])
            
            for i in range(0, Row_T):
                self.table_T.setItem(i, 4, QTableWidgetItem(str(round(EMP_U_Menos_T[i], 5)).replace('.', ',')))
                self.table_T.setItem(i, 5, QTableWidgetItem(str(round(EMP_U_Mas_T[i], 5)).replace('.', ',')))
                self.table_T.setItem(i, 6, QTableWidgetItem(Mensaje_T[i]))

            for i in range(0, Row_H):
                self.table_H.setItem(i, 4, QTableWidgetItem(str(round(EMP_U_Menos_H[i], 5)).replace('.', ',')))
                self.table_H.setItem(i, 5, QTableWidgetItem(str(round(EMP_U_Mas_H[i], 5)).replace('.', ',')))
                self.table_H.setItem(i, 6, QTableWidgetItem(Mensaje_H[i]))

            #---------- Conclusiones finales ----------#

            Decision_T = True
            for i in range(0, Row_T):
                if (Mensaje_T[i] != "Aceptable"):
                    Decision_T = False
            
            Decision_H = True
            for i in range(0, Row_H):
                if (Mensaje_H[i] != "Aceptable"):
                    Decision_H = False

            if(Decision_T == True):
                Result_T = "El Termohigrómetro con "+self.comboBox_1.currentText()+" N° "+self.lineEdit_6.text()+ " de " + self.lineEdit_7.text() + " cumple con las especificaciones técnicas."
                self.textEdit_1.setText(Result_T)
            else:
                Result_T = "El Termohigrómetro con "+self.comboBox_1.currentText()+" N° "+self.lineEdit_6.text()+ " de " + self.lineEdit_7.text() + " no cumple con todas las especificaciones técnicas."
                self.textEdit_1.setText(Result_T)

            if(Decision_H == True):
                Result_H = "El Termohigrómetro con "+self.comboBox_1.currentText()+" N° "+self.lineEdit_6.text()+ " de " + self.lineEdit_7.text() + " cumple con las especificaciones técnicas."
                self.textEdit_2.setText(Result_H)
            else:
                Result_H = "El Termohigrómetro con "+self.comboBox_1.currentText()+" N° "+self.lineEdit_6.text()+ " de " + self.lineEdit_7.text() + " no cumple con todas las especificaciones técnicas."
                self.textEdit_2.setText(Result_H)

            Fecha = str(datetime.date.today())

            Valor_1 = self.MakeTemohigrometros(LinesEdits, ComboBoxs, SpinBoxs, Indicacion_T, Correccion_T,
                                                Incertidumbre_T, EMP_T, Indicacion_H, Correccion_H, Incertidumbre_H,
                                                EMP_H, EMP_U_Menos_T, EMP_U_Mas_T, Mensaje_T, EMP_U_Menos_H, EMP_U_Mas_H, 
                                                Mensaje_H, Result_T, Result_H, Fecha)
            
            Valor_2 = self.GuardarFile()

            if (Valor_1 == True and Valor_2 == True):
                mesa="Correcto!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Information)
                msg.setText('Documento generado satisfactoriamente')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()
            elif (Valor_1 == True and Valor_2 == False):
                mesa="Revisar Información!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Warning)
                msg.setText('Se generó el documento pero no se guardó el archivo')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()
            elif (Valor_2 == True and Valor_1 == False):
                mesa="Revisar Información!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Warning)
                msg.setText('No se generó el documento pero sí se guardó el archivo.')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()
            else:
                mesa="Error!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Critical)
                msg.setText('Revisar toda la información suministrada')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()

        except Exception as e:
            print(e)
    
    def MakeTemohigrometros(self, LinesEdits, ComboBoxs, SpinBoxs, Indicacion_T, Correccion_T,
                            Incertidumbre_T, EMP_T, Indicacion_H, Correccion_H, Incertidumbre_H,
                            EMP_H, EMP_U_Menos_T, EMP_U_Mas_T, Mensaje_T, EMP_U_Menos_H, EMP_U_Mas_H, 
                            Mensaje_H, Result_T, Result_H, Fecha):
        try:
            registerFont (TTFont ('Arial', 'ARIAL.ttf'))
            registerFont (TTFont ('Book', 'BOOKOSB.ttf'))
            registerFont (TTFont ('Arial-Bold', 'ARIALBd.ttf'))

            canv = PMCanvas (10,10)            
            fileName = self.ruta + "\\Termohigrometros\\Documentos\\Confirmacion metrologica-" + ComboBoxs[0] + "-" + LinesEdits[0] + ".pdf"
            c = canvas.Canvas(fileName, pagesize=A4)
            p = ParagraphStyle(fileName)
            p.textColor = 'black'
            p.alignment = TA_JUSTIFY

            c.drawImage(self.ruta + "\\Base\\header_termohigrometro.png", 20, 760, 550, 50)

            tamle=int(9)
            c.setFont ('Arial-Bold', tamle)

            c.drawString(405, 740, "Fecha: ")
            c.drawString(450, 740, Fecha) 
            c.drawString(50, 710, "1.- Equipo / Instrumento de Medición ") 
            c.drawString(350, 710, "Calibración vigente: ") 

            c.setFont ('Arial', tamle)
            c.drawString(60, 695, "Cód. Identificación: ")
            c.drawString(160, 695, LinesEdits[0]) 
            c.drawString(60, 680, "Equipo / Instrumento: ")
            c.drawString(160, 680, LinesEdits[1]) 
            c.drawString(60, 665, "Alcance de Indicación: ")
            c.drawString(160, 665, LinesEdits[2]) 
            c.drawString(60, 650, "Resolución: ")
            c.drawString(160, 650, LinesEdits[3] + " °C") 

            c.drawString(350, 695, "Documento: ")
            c.drawString(450, 695, ComboBoxs[0])
            c.drawString(350, 680, "Número: ")
            c.drawString(450, 680, LinesEdits[4])
            c.drawString(350, 665, "Institución: ")
            c.drawString(450, 665, LinesEdits[5])
            c.drawString(350, 650, "Fecha de calibración: ")
            c.drawString(450, 650, LinesEdits[6])
            
            c.setFont ('Arial-Bold', tamle)
            c.drawString(50, 630, "2.- Evaluación de Resultados")

            Titulo = [['Indicación \n ( °C )', 'Corrección \n ( °C )', 'Incert. \n ( °C )', 'E.M.P. \n ( °C )']]

            table = Table(Titulo, [55, 55, 55, 55])
            table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                    ]))
            table.wrapOn(c, 10, 140)
            table.drawOn(c, 60, 590)

            Titulo = [['| E.M.P. | - U \n ( °C )', '| E.M.P. | + U \n ( °C )', 'Confirmación \n Metrológica']]

            table = Table(Titulo, [75, 75, 75])
            table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                    ]))
            table.wrapOn(c, 10, 140)
            table.drawOn(c, 340, 590)
            
            List_Indicacion_T = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in Indicacion_T.tolist()]
            List_Correccion_T = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in Correccion_T.tolist()]
            List_Incertidumbre_T = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in Incertidumbre_T.tolist()]
            List_EMP_T = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in EMP_T.tolist()]
            List_EMP_U_Menos_T = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in EMP_U_Menos_T.tolist()]
            List_EMP_U_Mas_T = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in EMP_U_Mas_T.tolist()]
            List_Mensaje_T = [[x] for x in Mensaje_T.tolist()]


            Rows_T = self.table_T.rowCount()
            Columns_T = self.table_T.columnCount()


            table = Table(List_Indicacion_T, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 60, 551 - (Rows_T - 3)*13)


            table = Table(List_Correccion_T, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 115, 551 - (Rows_T - 3)*13) 


            table = Table(List_Incertidumbre_T, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 170, 551 - (Rows_T - 3)*13) 



            table = Table(List_EMP_T, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 225, 551 - (Rows_T - 3)*13) 


            table = Table(List_EMP_U_Menos_T, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 340, 551 - (Rows_T - 3)*13) 


            table = Table(List_EMP_U_Mas_T, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 415, 551 - (Rows_T - 3)*13)


            table = Table(List_Mensaje_T, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 490, 551 - (Rows_T - 3)*13)
            
            ux = 551 - (Rows_T - 3)*13

            if (ux - 20 > 200):

                c.drawString(50, ux - 20, "3.- Criterio de Evaluación y Aceptación. ")

                data = [                
                        ['Criterio de Evaluación', 'Criterio de Aceptación'],
                        ['| E |  ≤  | E.M.P. | – U', 'Aceptable'],
                        ['| E.M.P. | – U  < | E |  ≤  | E.M.P. | + U', 'Indeterminado'],
                        ['| E.M.P. | + U  < | E |', 'No Aceptable'],
                    ]

                table = Table(data, [170, 150])
                table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('TEXTCOLOR',(0,-2),(-1,-1),colors.blue),
                    ('TEXTCOLOR',(0,-1),(-1,-1),colors.red),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'), #(0,-1),(-1,-1)
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER') #(0,0), (-1,-1)
                    ]))
                
                table .wrapOn(c, 10, 140)
                table .drawOn(c, 60, ux - 105)

                c.setFont ('Arial', tamle)

                c.drawString(400, ux - 105 + 60, "Donde: ")
                c.drawString(400, ux - 105 + 40, "E    :  Error / Corrección / Desviación")
                c.drawString(400, ux - 105 + 20, "E.M.P:  Error Máximo Permitido")
                c.drawString(400, ux - 105, "U    :  Incertidumbre Expandida")

            else:
                c.showPage()

                ux = 780

                c.drawString(50, ux - 20, "3.- Criterio de Evaluación y Aceptación. ")

                data = [                
                        ['Criterio de Evaluación', 'Criterio de Aceptación'],
                        ['| E |  ≤  | E.M.P. | – U', 'Aceptable'],
                        ['| E.M.P. | – U  < | E |  ≤  | E.M.P. | + U', 'Indeterminado'],
                        ['| E.M.P. | + U  < | E |', 'No Aceptable'],
                    ]

                table = Table(data, [170, 150])
                table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('TEXTCOLOR',(0,-2),(-1,-1),colors.blue),
                    ('TEXTCOLOR',(0,-1),(-1,-1),colors.red),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'), #(0,-1),(-1,-1)
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER') #(0,0), (-1,-1)
                    ]))
                
                table .wrapOn(c, 10, 140)
                table .drawOn(c, 60, ux - 105)

                c.setFont ('Arial', tamle)

                c.drawString(400, ux - 105 + 60, "Donde: ")
                c.drawString(400, ux - 105 + 40, "E    :  Error / Corrección / Desviación")
                c.drawString(400, ux - 105 + 20, "E.M.P:  Error Máximo Permitido")
                c.drawString(400, ux - 105, "U    :  Incertidumbre Expandida")

            uy = ux - 105
                        
            self.Grafica(Indicacion_T, Correccion_T, EMP_U_Mas_T, 
                        EMP_U_Menos_T, EMP_T, int(SpinBoxs[0]), 
                        "Termómetro " + self.lineEdit_2.text(), "T")

            if (uy > 300):
                c.drawImage('ConfirmacionT.png', 20, uy - 245, 570, 240)
            
            else:
                c.showPage()
                uy = 780
                c.drawImage('ConfirmacionT.png', 20, uy - 245, 570, 240)

            uz = uy - 245

            print(uz)
            
            p = ParagraphStyle(fileName)
            p.textColor = 'black'
            p.alignment = TA_JUSTIFY
            p.fontName ='Arial-Bold'
            p.fontSize = 8
            p.leading = 11.2

            para = Paragraph(self.textEdit_1.toPlainText(), p)
            para.wrapOn(c, 450, 200)
            N_reglon_1 = para.height / 11.2

            try:
                if (uz > 170):            
                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(50, uz - 20, "4.- Conclusión:")
                    u1 = uz - 40 - int(11*(N_reglon_1-1))
                    para.drawOn(c, 60, u1)

                    c.setFont ('Arial', tamle)
                    c.drawString(250, u1 - 50, "Supervisado por:")
                    c.line(330, u1 - 60, 480, u1 - 60)

                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(350, u1 - 70, ComboBoxs[1])

                    c.drawImage(self.ruta + "\\Base\\" + ComboBoxs[1] + ".png", 340, u1 - 50, 140, 50)
                    
                    
                else:
                    c.showPage()
                    uz = 780
                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(50, uz - 20, "4.- Conclusión:")
                    u1 = uz - 40 - int(11*(N_reglon_1-1))
                    para.drawOn(c, 60, u1)

                    c.setFont ('Arial', tamle)
                    c.drawString(250, u1 - 50, "Supervisado por:")
                    c.line(330, u1 - 60, 480, u1 - 60)

                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(350, u1 - 70, ComboBoxs[1])

                    c.drawImage(self.ruta + "\\Base\\" + ComboBoxs[1] + ".png", 340, u1 - 50, 140, 50)

            except:
                mensaje = "Falta Firma"
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setText("No existe la firma del supervisor")
                msg.setInformativeText(mensaje)
                msg.setStyleSheet("""background-color:#12abc4; color: white;""")
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()

            c.showPage()

            c.drawImage(self.ruta + "\\Base\\header_termohigrometro.png", 20, 760, 550, 50)

            tamle=int(9)
            c.setFont ('Arial-Bold', tamle)

            c.drawString(405, 740, "Fecha: ")
            c.drawString(450, 740, Fecha) 
            c.drawString(50, 710, "1.- Equipo / Instrumento de Medición ") 
            c.drawString(350, 710, "Calibración vigente: ") 
            
            c.setFont ('Arial', tamle)
            c.drawString(60, 695, "Cód. Identificación: ")
            c.drawString(160, 695, LinesEdits[7]) 
            c.drawString(60, 680, "Equipo / Instrumento: ")
            c.drawString(160, 680, LinesEdits[8]) 
            c.drawString(60, 665, "Alcance de Indicación: ")
            c.drawString(160, 665, LinesEdits[9]) 
            c.drawString(60, 650, "Resolución: ")
            c.drawString(160, 650, LinesEdits[10] + " %HR") 

            c.drawString(350, 695, "Documento: ")
            c.drawString(450, 695, LinesEdits[11])
            c.drawString(350, 680, "Número: ")
            c.drawString(450, 680, LinesEdits[12])
            c.drawString(350, 665, "Institución: ")
            c.drawString(450, 665, LinesEdits[13])
            c.drawString(350, 650, "Fecha de calibración: ")
            c.drawString(450, 650, LinesEdits[14])

            c.setFont ('Arial-Bold', tamle)
            c.drawString(50, 630, "2.- Evaluación de Resultados")

            Titulo = [['Indicación \n ( %HR )', 'Corrección \n ( %HR )', 'Incert. \n ( %HR )', 'E.M.P. \n ( %HR )']]

            table = Table(Titulo, [55, 55, 55, 55])
            table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                    ]))
            table.wrapOn(c, 10, 140)
            table.drawOn(c, 60, 590)

            Titulo = [['| E.M.P. | - U \n ( %HR )', '| E.M.P. | + U \n ( %HR )', 'Confirmación \n Metrológica']]

            table = Table(Titulo, [75, 75, 75])
            table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                    ]))
            table.wrapOn(c, 10, 140)
            table.drawOn(c, 340, 590)

            List_Indicacion_H = [[Coma.Deci2(x, int(SpinBoxs[1]))] for x in Indicacion_H.tolist()]
            List_Correccion_H = [[Coma.Deci2(x, int(SpinBoxs[1]))] for x in Correccion_H.tolist()]
            List_Incertidumbre_H = [[Coma.Deci2(x, int(SpinBoxs[1]))] for x in Incertidumbre_H.tolist()]
            List_EMP_H = [[Coma.Deci2(x, int(SpinBoxs[1]))] for x in EMP_H.tolist()]
            List_EMP_U_Menos_H = [[Coma.Deci2(x, int(SpinBoxs[1]))] for x in EMP_U_Menos_H.tolist()]
            List_EMP_U_Mas_H = [[Coma.Deci2(x, int(SpinBoxs[1]))] for x in EMP_U_Mas_H.tolist()]
            List_Mensaje_H = [[x] for x in Mensaje_H.tolist()]


            Rows_H = self.table_H.rowCount()
            Columns_H = self.table_H.columnCount()

            table = Table(List_Indicacion_H, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 60, 551 - (Rows_H - 3)*13)


            table = Table(List_Correccion_H, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 115, 551 - (Rows_H - 3)*13) 


            table = Table(List_Incertidumbre_H, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 170, 551 - (Rows_H - 3)*13) 



            table = Table(List_EMP_H, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 225, 551 - (Rows_H - 3)*13) 


            table = Table(List_EMP_U_Menos_H, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 340, 551 - (Rows_H - 3)*13) 


            table = Table(List_EMP_U_Mas_H, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 415, 551 - (Rows_H - 3)*13)


            table = Table(List_Mensaje_H, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 490, 551 - (Rows_H - 3)*13)
            
            ux = 551 - (Rows_H - 3)*13

            if (ux - 20 > 200):

                c.drawString(50, ux - 20, "3.- Criterio de Evaluación y Aceptación. ")

                data = [                
                        ['Criterio de Evaluación', 'Criterio de Aceptación'],
                        ['| E |  ≤  | E.M.P. | – U', 'Aceptable'],
                        ['| E.M.P. | – U  < | E |  ≤  | E.M.P. | + U', 'Indeterminado'],
                        ['| E.M.P. | + U  < | E |', 'No Aceptable'],
                    ]

                table = Table(data, [170, 150])
                table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('TEXTCOLOR',(0,-2),(-1,-1),colors.blue),
                    ('TEXTCOLOR',(0,-1),(-1,-1),colors.red),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'), #(0,-1),(-1,-1)
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER') #(0,0), (-1,-1)
                    ]))
                
                table .wrapOn(c, 10, 140)
                table .drawOn(c, 60, ux - 105)

                c.setFont ('Arial', tamle)

                c.drawString(400, ux - 105 + 60, "Donde: ")
                c.drawString(400, ux - 105 + 40, "E    :  Error / Corrección / Desviación")
                c.drawString(400, ux - 105 + 20, "E.M.P:  Error Máximo Permitido")
                c.drawString(400, ux - 105, "U    :  Incertidumbre Expandida")

            else:
                c.showPage()

                ux = 780

                c.drawString(50, ux - 20, "3.- Criterio de Evaluación y Aceptación. ")

                data = [                
                        ['Criterio de Evaluación', 'Criterio de Aceptación'],
                        ['| E |  ≤  | E.M.P. | – U', 'Aceptable'],
                        ['| E.M.P. | – U  < | E |  ≤  | E.M.P. | + U', 'Indeterminado'],
                        ['| E.M.P. | + U  < | E |', 'No Aceptable'],
                    ]

                table = Table(data, [170, 150])
                table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('TEXTCOLOR',(0,-2),(-1,-1),colors.blue),
                    ('TEXTCOLOR',(0,-1),(-1,-1),colors.red),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'), #(0,-1),(-1,-1)
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER') #(0,0), (-1,-1)
                    ]))
                
                table .wrapOn(c, 10, 140)
                table .drawOn(c, 60, ux - 105)

                c.setFont ('Arial', tamle)

                c.drawString(400, ux - 105 + 60, "Donde: ")
                c.drawString(400, ux - 105 + 40, "E    :  Error / Corrección / Desviación")
                c.drawString(400, ux - 105 + 20, "E.M.P:  Error Máximo Permitido")
                c.drawString(400, ux - 105, "U    :  Incertidumbre Expandida")

            uy = ux - 105

            self.Grafica(Indicacion_H, Correccion_H, EMP_U_Mas_H, 
                        EMP_U_Menos_H, EMP_H, int(SpinBoxs[1]), 
                        "Higrómetro " + self.lineEdit_9.text(), "H")

            if (uy > 300):
                c.drawImage('ConfirmacionH.png', 20, uy - 245, 570, 240)
            
            else:
                c.showPage()
                uy = 780
                c.drawImage('ConfirmacionH.png', 20, uy - 245, 570, 240)

            uz = uy - 245

            print(uz)
            
            p = ParagraphStyle(fileName)
            p.textColor = 'black'
            p.alignment = TA_JUSTIFY
            p.fontName ='Arial-Bold'
            p.fontSize = 8
            p.leading = 11.2

            para = Paragraph(self.textEdit_2.toPlainText(), p)
            para.wrapOn(c, 450, 200)
            N_reglon_1 = para.height / 11.2

            try:
                if (uz > 170):            
                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(50, uz - 20, "4.- Conclusión:")
                    u1 = uz - 40 - int(11*(N_reglon_1-1))
                    para.drawOn(c, 60, u1)

                    c.setFont ('Arial', tamle)
                    c.drawString(250, u1 - 50, "Supervisado por:")
                    c.line(330, u1 - 60, 480, u1 - 60)

                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(350, u1 - 70, ComboBoxs[1])

                    c.drawImage(self.ruta + "\\Base\\" + ComboBoxs[1] + ".png", 340, u1 - 50, 140, 50)
                    
                    
                else:
                    c.showPage()
                    uz = 780
                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(50, uz - 20, "4.- Conclusión:")
                    u1 = uz - 40 - int(11*(N_reglon_1-1))
                    para.drawOn(c, 60, u1)

                    c.setFont ('Arial', tamle)
                    c.drawString(250, u1 - 50, "Supervisado por:")
                    c.line(330, u1 - 60, 480, u1 - 60)

                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(350, u1 - 70, ComboBoxs[1])

                    c.drawImage(self.ruta + "\\Base\\" + ComboBoxs[1] + ".png", 340, u1 - 50, 140, 50)

            except:
                mensaje = "Falta Firma"
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setText("No existe la firma del supervisor")
                msg.setInformativeText(mensaje)
                msg.setStyleSheet("""background-color:#12abc4; color: white;""")
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()

            c.save()
            
            Info = True


        except Exception as e:
            Info = False
            print(e)
        
        return Info
    
    def Grafica(self, Indicacion, Correccion, EMPMas, EMPMenos, EMP, Redondeo, Nombre, Tipo):
        try:            
            def comma_formatter(x, pos):
                if Redondeo == 0:
                    return "{:,.0f}".format(x).replace(".", ",")
                elif Redondeo == 1:
                    return "{:,.1f}".format(x).replace(".", ",")
                elif Redondeo == 2:
                    return "{:,.2f}".format(x).replace(".", ",")
                elif Redondeo == 3:
                    return "{:,.3f}".format(x).replace(".", ",")
                elif Redondeo == 4:
                    return "{:,.4f}".format(x).replace(".", ",")
                elif Redondeo == 5:
                    return "{:,.5f}".format(x).replace(".", ",")
                elif Redondeo == 6:
                    return "{:,.6f}".format(x).replace(".", ",")
                elif Redondeo == 7:
                    return "{:,.7f}".format(x).replace(".", ",")
                elif Redondeo == 8:
                    return "{:,.8f}".format(x).replace(".", ",")

            plt.figure(figsize=(12, 5))
            plt.errorbar(Indicacion, Correccion, yerr=0, fmt="d-", color='b', lw=1 ,label="Corrección")
            plt.errorbar(Indicacion, EMP, yerr=0, fmt="-", color='k', lw=1 ,label="+ EMP")
            plt.errorbar(Indicacion, -1*EMP, yerr=0, fmt="-", color='k', lw=1 ,label="- EMP")
            plt.errorbar(Indicacion, EMPMas, yerr=0, fmt="-", color='r', lw=1 ,label="EMP + U")
            plt.errorbar(Indicacion, EMPMenos, yerr=0, fmt="-", color='r', lw=1 ,label="EMP - U")
            plt.errorbar(Indicacion, -1*EMPMas, yerr=0, fmt="-", color='r', lw=1 ,label="- EMP - U")
            plt.errorbar(Indicacion, -1*EMPMenos, yerr=0, fmt="-", color='r', lw=1 ,label="- EMP + U")    
            plt.gca().yaxis.set_major_formatter(FuncFormatter(comma_formatter)) 
            plt.gca().xaxis.set_major_formatter(FuncFormatter(comma_formatter))     
            grid(True)  
            ylim(-2*max(EMPMas),2*max(EMPMas))

            if Tipo == "T":
                ylabel("Corrección ( °C )")
            elif Tipo == "H":
                ylabel("Corrección ( %HR )")
            elif Tipo == "C":
                ylabel("Corrección ( s )")
            elif Tipo == "P":
                ylabel("Corrección ( g )")
            else:
                Info = str(Tipo).split(' ')
                ylabel("Corrección ( "+Info[1]+" )")



            for i in range(0, len(Indicacion)):
                text(Indicacion[i], Correccion[i], "  "+Coma.Deci2(Correccion[i], Redondeo), color='b')
                        
            title("Confirmación Metrológica - " + Nombre)
            legend(loc='upper center', bbox_to_anchor=(0.5, -0.05), fancybox=True, shadow=True, ncol=10)

            if Tipo == "T":
                try:
                    os.remove('ConfirmacionT.png')
                except:
                    pass
                savefig('ConfirmacionT.png')
                
            elif Tipo == "H":
                try:
                    os.remove('ConfirmacionH.png')
                except:
                    pass                
                savefig('ConfirmacionH.png')
            
            elif Tipo == "C":
                try:
                    os.remove('ConfirmacionC.png')
                except:
                    pass                
                savefig('ConfirmacionC.png')
            
            elif Tipo == "P":
                try:
                    os.remove('ConfirmacionP.png')
                except:
                    pass                
                savefig('ConfirmacionP.png')
            else:
                Info = str(Tipo).split(' ')
                if (Info[0] == "M"):
                    try:
                        os.remove('ConfirmacionM.png')
                    except:
                        pass                
                    savefig('ConfirmacionM.png')
                

            
            

        except Exception as e:
            print(e)
    
    def Refresh(self):
        self.hide()
        self.ui = Ui_Main()
        self.ui.show()
    
    def AddFila(self):
        Row_T = self.table_T.rowCount()
        Row_H = self.table_H.rowCount()

        self.table_T.setRowCount(Row_T + 1)
        self.table_H.setRowCount(Row_H + 1)
    
    def AbrirFile(self):
        try:
            filename = QFileDialog.getOpenFileName(None, 'Abrir Archivos', filter='Lab Files (*.cmil)',
                                                   options=QFileDialog.Options())
            
            if filename[0]:
                for i in range(2, 17):
                    getattr(self, "lineEdit_{}".format(i)).setText("")

                for i in range(1, 3):
                    getattr(self, "comboBox_{}".format(i)).setItemText(0, "")
                    getattr(self, "comboBox_{}".format(i)).setCurrentIndex(0)
                
                for i in range(1, 3):
                    getattr(self, "spinBox_{}".format(i)).setValue(5)
                
                for i in range(1, 3):
                    getattr(self, "textEdit_{}".format(i)).setText("")
                
                self.table_T.clearContents()
                self.table_H.clearContents()

                with open(filename[0], 'r', encoding='utf-8') as Info:

                    for i in range(2, 17):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "lineEdit_{}".format(i)).setText(val[0])
                    
                    for i in range(1, 3):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "comboBox_{}".format(i)).setItemText(0, val[0])     
                        getattr(self, "comboBox_{}".format(i)).setCurrentIndex(0)    

                    for i in range(1, 3):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "spinBox_{}".format(i)).setValue(int(val[0])) 
                    
                    for i in range(1, 3):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "textEdit_{}".format(i)).setText(val[0])
                    
                    val = str(Info.readline()).split('\n')
                    Row_T = int(val[0])

                    self.table_T.setRowCount(Row_T)                    

                    val = str(Info.readline()).split('\n')
                    Row_H = int(val[0])

                    self.table_H.setRowCount(Row_H)

                    for i in range(0, Row_T):
                        for j in range(0, 7):
                            val = str(Info.readline()).split('\n')
                            item = QTableWidgetItem(val[0])
                            self.table_T.setItem(i, j, item)
                    
                    for i in range(0, Row_H):
                        for j in range(0, 7):
                            val = str(Info.readline()).split('\n')
                            item = QTableWidgetItem(val[0])
                            self.table_H.setItem(i, j, item)

        except Exception as e:
            print(e)

    def GuardarInfo(self):

        Info = self.GuardarFile()

        if Info == True:
            mesa="Correcto!"
            msg = QMessageBox()
            msg.setStyleSheet("""background-color: #12abc4;
                                    color: white;            
            """)
            msg.setIcon(QMessageBox.Information)
            msg.setText('Archivo guardado satisfactoriamente')
            msg.setInformativeText(mesa)
            msg.setWindowTitle("METROIL S.A.C.")
            msg.exec_()
        else:
            mesa="Error!"
            msg = QMessageBox()
            msg.setStyleSheet("""background-color: #12abc4;
                                    color: white;            
            """)
            msg.setIcon(QMessageBox.Critical)
            msg.setText('Colocar código del termohigrómetro')
            msg.setInformativeText(mesa)
            msg.setWindowTitle("METROIL S.A.C.")
            msg.exec_()

    def GuardarFile(self):
        try:
            Ano = str(datetime.date.today()).split("-")
            Ano = Ano[0]            
            
            nom1 = "Termohigrometro"
            nom2 = self.lineEdit_2.text()
            nom3 = Ano

            if (nom1 != "" and nom2 != "" and nom3 != ""):
                self.NameG = nom1+"-"+nom2+"-"+nom3+".cmil"
                Info = open(self.ruta + "\\Termohigrometros\\Archivos Files\\" + self.NameG, 'w',encoding='utf-8')

                for i in range(2, 17):
                    Info.write(str(getattr(self, "lineEdit_{}".format(i)).text()) + '\n')
                
                for i in range(1, 3):
                    Info.write(str(getattr(self, "comboBox_{}".format(i)).currentText()) + '\n')
                
                for i in range(1, 3):
                    Info.write(str(getattr(self, "spinBox_{}".format(i)).value()) + '\n')
                
                for i in range(1, 3):
                    Info.write(str(getattr(self, "textEdit_{}".format(i)).toPlainText()) + '\n')
                
                Row_T = self.table_T.rowCount()
                Row_H = self.table_H.rowCount()

                Info.write(str(Row_T)+ '\n')
                Info.write(str(Row_H)+ '\n')

                rowdata = []
                for row in range(self.table_T.rowCount()):                        
                    for column in range(self.table_T.columnCount()):
                        item = self.table_T.item(row, column)                        
                        if item is not None:
                            rowdata.append(item.text())
                        else:
                            rowdata.append('')
                
                for i in range(0, len(rowdata)):
                    Info.write(str(rowdata[i])+'\n')
                
                rowdata = []
                for row in range(self.table_H.rowCount()):                        
                    for column in range(self.table_H.columnCount()):
                        item = self.table_H.item(row, column)                        
                        if item is not None:
                            rowdata.append(item.text())
                        else:
                            rowdata.append('')
                
                for i in range(0, len(rowdata)):
                    Info.write(str(rowdata[i])+'\n')
                
                Reenviar = True

            else:
                Reenviar = False

            return Reenviar

        except Exception as e:
            print(e)

    def AgrandarMenuLateral(self):
        if True:
            width = self.leftMenuContenedor.width()
            norm = 65
            if width == 65:
                exten = 200
            else:
                exten = norm

            self.ani = QPropertyAnimation(self.leftMenuContenedor, b'minimumWidth')
            self.ani.setDuration(300)
            self.ani.setStartValue(width)
            self.ani.setEndValue(exten)
            self.ani.setEasingCurve(QtCore.QEasingCurve.InOutQuart)
            self.ani.start()

class Ui_Main_2(QMainWindow):
    def __init__(self):        
        super(Ui_Main_2, self).__init__()
        uic.loadUi("Cronometros.ui", self)        

        self.setWindowIcon(QIcon('0.ico'))
        
        try:
            ruta_1 = open("ruta.rut",'r', encoding='utf-8')
            ruta = ruta_1.readline()
            self.ruta = ruta
            self.ruta = r'{}'.format(self.ruta)
            ruta_1.close()
        except Exception as e:
            print(e)

        unid = open(self.ruta+"\\Base\\Supervisores.til",'r',encoding='utf-8')        
        unida = unid.readlines()            
        unid.close()

        for i in range(0, len(unida)):
            valunida = unida[i].split('\n')                
            self.comboBox_2.addItem(valunida[0])
        self.comboBox_2.setCurrentIndex(1)


        self.table = QTableWidget(0, 7)

        self.table.setHorizontalHeaderLabels(['Indicación \n (s)',
                                                'Corrección \n (s)',
                                                'Incertidumbre \n (s)',
                                                'E.M.P. \n ( ± s)',
                                                '|E.M.P.| - U \n (s)',
                                                '|E.M.P.| + U \n (s)',
                                                'Conclusión'])
        self.table.horizontalHeader().setStyleSheet("::section{Background-color: #1c3048}")
        self.table.setSelectionBehavior(QTableWidget.SelectRows)
        self.table.setSelectionMode(QTableWidget.SingleSelection) 
        header = self.table.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.Stretch)
        self.table.verticalHeader().setVisible(False)
        self.table.setRowCount(3)

        self.gridLayout_4.addWidget(self.table)

        self.menuBtn.clicked.connect(self.AgrandarMenuLateral)

        self.RetornarBtn.clicked.connect(self.Regresar)
        self.FilaBtn.clicked.connect(self.AddFila)
        self.RefrescarBtn.clicked.connect(self.Refresh)
        self.GuardarBtn.clicked.connect(self.GuardarInfo)
        self.GenerarBtn.clicked.connect(self.Generar)
        self.AbrirBtn.clicked.connect(self.AbrirFile)
        self.MenosBtn.clicked.connect(self.DeleteFile)
        self.informacionBtn.clicked.connect(self.Information)

    def Information(self):
        Ui_Informacion().exec_()

    def DeleteFile(self):
        Row = self.table.rowCount()
        self.table.setRowCount(Row - 1)

    def AbrirFile(self):
        try:
            filename = QFileDialog.getOpenFileName(None, 'Abrir Archivos', filter='Lab Files (*.cmil)',
                                                   options=QFileDialog.Options())
            
            if filename[0]:
                for i in range(2, 9):
                    getattr(self, "lineEdit_{}".format(i)).setText("")

                for i in range(1, 3):
                    getattr(self, "comboBox_{}".format(i)).setItemText(0, "")
                    getattr(self, "comboBox_{}".format(i)).setCurrentIndex(0)
                
                for i in range(1, 2):
                    getattr(self, "spinBox_{}".format(i)).setValue(5)
                
                for i in range(1, 2):
                    getattr(self, "textEdit_{}".format(i)).setText("")
                
                self.table.clearContents()

                with open(filename[0], 'r', encoding='utf-8') as Info:

                    for i in range(2, 9):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "lineEdit_{}".format(i)).setText(val[0])
                    
                    for i in range(1, 3):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "comboBox_{}".format(i)).setItemText(0, val[0])     
                        getattr(self, "comboBox_{}".format(i)).setCurrentIndex(0)    

                    for i in range(1, 2):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "spinBox_{}".format(i)).setValue(int(val[0])) 
                    
                    for i in range(1, 2):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "textEdit_{}".format(i)).setText(val[0])
                    
                    val = str(Info.readline()).split('\n')
                    Row = int(val[0])

                    self.table.setRowCount(Row)

                    for i in range(0, Row):
                        for j in range(0, 7):
                            val = str(Info.readline()).split('\n')
                            item = QTableWidgetItem(val[0])
                            self.table.setItem(i, j, item)
                   
        except Exception as e:
            print(e)

    def Generar(self):
        try:
            LinesEdits = np.zeros(7, dtype = object)
            for i in range(2, 9):
                LinesEdits[i-2] = getattr(self, "lineEdit_{}".format(i)).text()

            ComboBoxs = np.zeros(2, dtype = object)
            for i in range(1, 3):
                ComboBoxs[i-1] = getattr(self, "comboBox_{}".format(i)).currentText()
            
            SpinBoxs = np.zeros(1)
            for i in range(1, 2):
                SpinBoxs[i-1] = getattr(self, "spinBox_{}".format(i)).value()
            
            Row = self.table.rowCount()

            Indicacion = np.zeros(Row)
            Correccion = np.zeros(Row)
            Incertidumbre = np.zeros(Row)
            EMP = np.zeros(Row)

            for i in range(0, Row):
                item = self.table.item(i, 0)
                valor = float(str(item.text()).replace(',', '.'))
                Indicacion[i] = valor

                item = self.table.item(i, 1)
                valor = float(str(item.text()).replace(',', '.'))
                Correccion[i] = valor

                item = self.table.item(i, 2)
                valor = float(str(item.text()).replace(',', '.'))
                Incertidumbre[i] = valor

                item = self.table.item(i, 3)
                valor = float(str(item.text()).replace(',', '.'))
                EMP[i] = valor

            #------- Aplicar Cálculo -------#

            EMP_U_Menos = np.zeros(Row)
            EMP_U_Mas = np.zeros(Row)
            Mensaje = np.zeros(Row, dtype = object)

            for i in range(0, Row):
                EMP_U_Menos[i], EMP_U_Mas[i], Mensaje[i] = Ui_Main.Calculo(self, EMP[i], Incertidumbre[i], Correccion[i])

            for i in range(0, Row):
                self.table.setItem(i, 4, QTableWidgetItem(str(round(EMP_U_Menos[i], 5)).replace('.', ',')))
                self.table.setItem(i, 5, QTableWidgetItem(str(round(EMP_U_Mas[i], 5)).replace('.', ',')))
                self.table.setItem(i, 6, QTableWidgetItem(Mensaje[i]))

            #---------- Conclusiones finales ----------#

            Decision = True
            for i in range(0, Row):
                if (Mensaje[i] != "Aceptable"):
                    Decision = False
            
            if(Decision == True):
                Result = "El Cronómetro con "+self.comboBox_1.currentText()+" N° "+self.lineEdit_6.text()+ " de " + self.lineEdit_7.text() + " cumple con las especificaciones técnicas."
                self.textEdit_1.setText(Result)
            else:
                Result = "El Cronómetro con "+self.comboBox_1.currentText()+" N° "+self.lineEdit_6.text()+ " de " + self.lineEdit_7.text() + " no cumple con todas las especificaciones técnicas."
                self.textEdit_1.setText(Result)

            Fecha = str(datetime.date.today())

            Valor_1 = self.MakeCronometros(LinesEdits, ComboBoxs, SpinBoxs, Indicacion, Correccion,
                                           Incertidumbre, EMP, EMP_U_Menos, EMP_U_Mas, Mensaje, Result,
                                           Fecha)
            
            Valor_2 = self.GuardarFile()

            if (Valor_1 == True and Valor_2 == True):
                mesa="Correcto!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Information)
                msg.setText('Documento generado satisfactoriamente')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()
            
            elif (Valor_1 == True and Valor_2 == False):
                mesa="Revisar Información!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Warning)
                msg.setText('Se generó el documento pero no se guardó el archivo')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()
            
            elif (Valor_2 == True and Valor_1 == False):
                mesa="Revisar Información!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Warning)
                msg.setText('No se generó el documento pero sí se guardó el archivo.')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()
            
            else:
                mesa="Error!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Critical)
                msg.setText('Revisar toda la información suministrada')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()


        except Exception as e:
            print(e)
    
    def MakeCronometros(self, LinesEdits, ComboBoxs, SpinBoxs, Indicacion, Correccion,
                        Incertidumbre, EMP, EMP_U_Menos, EMP_U_Mas, Mensaje, Result,
                        Fecha):
        try:
            registerFont (TTFont ('Arial', 'ARIAL.ttf'))
            registerFont (TTFont ('Book', 'BOOKOSB.ttf'))
            registerFont (TTFont ('Arial-Bold', 'ARIALBd.ttf'))

            canv = PMCanvas (10,10)            
            fileName = self.ruta + "\\Cronometros\\Documentos\\Confirmacion metrologica-" + ComboBoxs[0] + "-" + LinesEdits[0] + ".pdf"
            c = canvas.Canvas(fileName, pagesize=A4)
            p = ParagraphStyle(fileName)
            p.textColor = 'black'
            p.alignment = TA_JUSTIFY

            c.drawImage(self.ruta + "\\Base\\header_cronometro.png", 20, 760, 550, 50)

            tamle=int(9)
            c.setFont ('Arial-Bold', tamle)

            c.drawString(405, 740, "Fecha: ")
            c.drawString(450, 740, Fecha) 
            c.drawString(50, 710, "1.- Equipo / Instrumento de Medición ") 
            c.drawString(350, 710, "Calibración vigente: ") 

            c.setFont ('Arial', tamle)
            c.drawString(60, 695, "Cód. Identificación: ")
            c.drawString(160, 695, LinesEdits[0]) 
            c.drawString(60, 680, "Equipo / Instrumento: ")
            c.drawString(160, 680, LinesEdits[1]) 
            c.drawString(60, 665, "Alcance de Indicación: ")
            c.drawString(160, 665, LinesEdits[2]) 
            c.drawString(60, 650, "Resolución: ")
            c.drawString(160, 650, LinesEdits[3] + " s") 

            c.drawString(350, 695, "Documento: ")
            c.drawString(450, 695, ComboBoxs[0])
            c.drawString(350, 680, "Número: ")
            c.drawString(450, 680, LinesEdits[4])
            c.drawString(350, 665, "Institución: ")
            c.drawString(450, 665, LinesEdits[5])
            c.drawString(350, 650, "Fecha de calibración: ")
            c.drawString(450, 650, LinesEdits[6])
            
            c.setFont ('Arial-Bold', tamle)
            c.drawString(50, 630, "2.- Evaluación de Resultados")

            Titulo = [['Indicación \n ( s )', 'Corrección \n ( s )', 'Incert. \n ( s )', 'E.M.P. \n ( ±s )']]

            table = Table(Titulo, [55, 55, 55, 55])
            table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                    ]))
            table.wrapOn(c, 10, 140)
            table.drawOn(c, 60, 590)

            Titulo = [['| E.M.P. | - U \n ( s )', '| E.M.P. | + U \n ( s )', 'Confirmación \n Metrológica']]

            table = Table(Titulo, [75, 75, 75])
            table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                    ]))
            table.wrapOn(c, 10, 140)
            table.drawOn(c, 340, 590)
            
            List_Indicacion = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in Indicacion.tolist()]
            List_Correccion = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in Correccion.tolist()]
            List_Incertidumbre = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in Incertidumbre.tolist()]
            List_EMP = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in EMP.tolist()]
            List_EMP_U_Menos = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in EMP_U_Menos.tolist()]
            List_EMP_U_Mas = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in EMP_U_Mas.tolist()]
            List_Mensaje = [[x] for x in Mensaje.tolist()]


            Rows = self.table.rowCount()
            Columns = self.table.columnCount()


            table = Table(List_Indicacion, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 60, 551 - (Rows - 3)*13)


            table = Table(List_Correccion, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 115, 551 - (Rows - 3)*13) 


            table = Table(List_Incertidumbre, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 170, 551 - (Rows - 3)*13) 



            table = Table(List_EMP, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 225, 551 - (Rows - 3)*13) 


            table = Table(List_EMP_U_Menos, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 340, 551 - (Rows - 3)*13) 


            table = Table(List_EMP_U_Mas, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 415, 551 - (Rows - 3)*13)


            table = Table(List_Mensaje, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 490, 551 - (Rows - 3)*13)
            
            ux = 551 - (Rows - 3)*13

            if (ux - 20 > 200):

                c.drawString(50, ux - 20, "3.- Criterio de Evaluación y Aceptación. ")

                data = [                
                        ['Criterio de Evaluación', 'Criterio de Aceptación'],
                        ['| E |  ≤  | E.M.P. | – U', 'Aceptable'],
                        ['| E.M.P. | – U  < | E |  ≤  | E.M.P. | + U', 'Indeterminado'],
                        ['| E.M.P. | + U  < | E |', 'No Aceptable'],
                    ]

                table = Table(data, [170, 150])
                table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('TEXTCOLOR',(0,-2),(-1,-1),colors.blue),
                    ('TEXTCOLOR',(0,-1),(-1,-1),colors.red),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'), #(0,-1),(-1,-1)
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER') #(0,0), (-1,-1)
                    ]))
                
                table .wrapOn(c, 10, 140)
                table .drawOn(c, 60, ux - 105)

                c.setFont ('Arial', tamle)

                c.drawString(400, ux - 105 + 60, "Donde: ")
                c.drawString(400, ux - 105 + 40, "E    :  Error / Corrección / Desviación")
                c.drawString(400, ux - 105 + 20, "E.M.P:  Error Máximo Permitido")
                c.drawString(400, ux - 105, "U    :  Incertidumbre Expandida")

            else:
                c.showPage()

                ux = 780

                c.drawString(50, ux - 20, "3.- Criterio de Evaluación y Aceptación. ")

                data = [                
                        ['Criterio de Evaluación', 'Criterio de Aceptación'],
                        ['| E |  ≤  | E.M.P. | – U', 'Aceptable'],
                        ['| E.M.P. | – U  < | E |  ≤  | E.M.P. | + U', 'Indeterminado'],
                        ['| E.M.P. | + U  < | E |', 'No Aceptable'],
                    ]

                table = Table(data, [170, 150])
                table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('TEXTCOLOR',(0,-2),(-1,-1),colors.blue),
                    ('TEXTCOLOR',(0,-1),(-1,-1),colors.red),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'), #(0,-1),(-1,-1)
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER') #(0,0), (-1,-1)
                    ]))
                
                table .wrapOn(c, 10, 140)
                table .drawOn(c, 60, ux - 105)

                c.setFont ('Arial', tamle)

                c.drawString(400, ux - 105 + 60, "Donde: ")
                c.drawString(400, ux - 105 + 40, "E    :  Error / Corrección / Desviación")
                c.drawString(400, ux - 105 + 20, "E.M.P:  Error Máximo Permitido")
                c.drawString(400, ux - 105, "U    :  Incertidumbre Expandida")

            uy = ux - 105
                        
            Ui_Main.Grafica(self, Indicacion, Correccion, EMP_U_Mas,
                            EMP_U_Menos, EMP, int(SpinBoxs[0]), 
                            "Cronómetro " + self.lineEdit_2.text(), "C")

            if (uy > 300):
                c.drawImage('ConfirmacionC.png', 20, uy - 245, 570, 240)
            
            else:
                c.showPage()
                uy = 780
                c.drawImage('ConfirmacionC.png', 20, uy - 245, 570, 240)

            uz = uy - 245
            
            
            p = ParagraphStyle(fileName)
            p.textColor = 'black'
            p.alignment = TA_JUSTIFY
            p.fontName ='Arial-Bold'
            p.fontSize = 8
            p.leading = 11.2

            para = Paragraph(self.textEdit_1.toPlainText(), p)
            para.wrapOn(c, 450, 200)
            N_reglon_1 = para.height / 11.2

            try:
                if (uz > 170):            
                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(50, uz - 20, "4.- Conclusión:")
                    u1 = uz - 40 - int(11*(N_reglon_1-1))
                    para.drawOn(c, 60, u1)

                    c.setFont ('Arial', tamle)
                    c.drawString(250, u1 - 50, "Supervisado por:")
                    c.line(330, u1 - 60, 480, u1 - 60)

                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(350, u1 - 70, ComboBoxs[1])

                    c.drawImage(self.ruta + "\\Base\\" + ComboBoxs[1] + ".png", 340, u1 - 50, 140, 50)
                    
                    
                else:
                    c.showPage()
                    uz = 780
                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(50, uz - 20, "4.- Conclusión:")
                    u1 = uz - 40 - int(11*(N_reglon_1-1))
                    para.drawOn(c, 60, u1)

                    c.setFont ('Arial', tamle)
                    c.drawString(250, u1 - 50, "Supervisado por:")
                    c.line(330, u1 - 60, 480, u1 - 60)

                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(350, u1 - 70, ComboBoxs[1])

                    c.drawImage(self.ruta + "\\Base\\" + ComboBoxs[1] + ".png", 340, u1 - 50, 140, 50)

            except:
                mensaje = "Falta Firma"
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setText("No existe la firma del supervisor")
                msg.setInformativeText(mensaje)
                msg.setStyleSheet("""background-color:#12abc4; color: white;""")
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()

            c.save()
            
            Info = True

        except Exception as e:
            Info = False
            print(e)
        
        return Info
    
    def GuardarInfo(self):

        Info = self.GuardarFile()

        if Info == True:
            mesa="Correcto!"
            msg = QMessageBox()
            msg.setStyleSheet("""background-color: #12abc4;
                                    color: white;            
            """)
            msg.setIcon(QMessageBox.Information)
            msg.setText('Archivo guardado satisfactoriamente')
            msg.setInformativeText(mesa)
            msg.setWindowTitle("METROIL S.A.C.")
            msg.exec_()
        else:
            mesa="Error!"
            msg = QMessageBox()
            msg.setStyleSheet("""background-color: #12abc4;
                                    color: white;            
            """)
            msg.setIcon(QMessageBox.Critical)
            msg.setText('Colocar código del termohigrómetro')
            msg.setInformativeText(mesa)
            msg.setWindowTitle("METROIL S.A.C.")
            msg.exec_()

    def GuardarFile(self):
        try:
            Ano = str(datetime.date.today()).split("-")
            Ano = Ano[0]            
            
            nom1 = "Cronometro"
            nom2 = self.lineEdit_2.text()
            nom3 = Ano

            if (nom1 != "" and nom2 != "" and nom3 != ""):
                self.NameG = nom1+"-"+nom2+"-"+nom3+".cmil"
                Info = open(self.ruta + "\\Cronometros\\Archivos Files\\" + self.NameG, 'w',encoding='utf-8')

                for i in range(2, 9):
                    Info.write(str(getattr(self, "lineEdit_{}".format(i)).text()) + '\n')
                
                for i in range(1, 3):
                    Info.write(str(getattr(self, "comboBox_{}".format(i)).currentText()) + '\n')
                
                for i in range(1, 2):
                    Info.write(str(getattr(self, "spinBox_{}".format(i)).value()) + '\n')
                
                for i in range(1, 2):
                    Info.write(str(getattr(self, "textEdit_{}".format(i)).toPlainText()) + '\n')
                
                Row = self.table.rowCount()

                Info.write(str(Row)+ '\n')

                rowdata = []
                for row in range(self.table.rowCount()):                        
                    for column in range(self.table.columnCount()):
                        item = self.table.item(row, column)                        
                        if item is not None:
                            rowdata.append(item.text())
                        else:
                            rowdata.append('')
                
                for i in range(0, len(rowdata)):
                    Info.write(str(rowdata[i])+'\n')
                
                Reenviar = True

            else:
                Reenviar = False

            return Reenviar

        except Exception as e:
            print(e)
    
    def Refresh(self):
        self.hide()
        self.ui = Ui_Main_2()
        self.ui.show()

    def Regresar(self):
        self.close()
        self.ui = Start()
        self.ui.show()
    
    def AddFila(self):
        Row = self.table.rowCount()

        self.table.setRowCount(Row + 1)        

    def AgrandarMenuLateral(self):
        if True:
            width = self.leftMenuContenedor.width()
            norm = 65
            if width == 65:
                exten = 200
            else:
                exten = norm

            self.ani = QPropertyAnimation(self.leftMenuContenedor, b'minimumWidth')
            self.ani.setDuration(300)
            self.ani.setStartValue(width)
            self.ani.setEndValue(exten)
            self.ani.setEasingCurve(QtCore.QEasingCurve.InOutQuart)
            self.ani.start()

class Ui_Main_3(QMainWindow):
    def __init__(self):        
        super(Ui_Main_3, self).__init__()
        uic.loadUi("Cronometros.ui", self)        

        self.setWindowIcon(QIcon('0.ico'))

        try:
            ruta_1 = open("ruta.rut",'r', encoding='utf-8')
            ruta = ruta_1.readline()
            self.ruta = ruta
            self.ruta = r'{}'.format(self.ruta)
            ruta_1.close()
        except Exception as e:
            print(e)

        unid = open(self.ruta+"\\Base\\Supervisores.til",'r',encoding='utf-8')        
        unida = unid.readlines()            
        unid.close()

        for i in range(0, len(unida)):
            valunida = unida[i].split('\n')                
            self.comboBox_2.addItem(valunida[0])
        self.comboBox_2.setCurrentIndex(1)

        self.lineEdit_3.setText("Pesas")
        self.label_5.setText("Alcance de Indicación:")
        self.label_6.setText("Resolución:")

        self.table = QTableWidget(0, 7)
        self.table.setHorizontalHeaderLabels(['Indicación \n (kg)',
                                                'Corrección \n (g)',
                                                'Incertidumbre \n (g)',
                                                'E.M.P. \n ( ± g)',
                                                '|E.M.P.| - U \n (g)',
                                                '|E.M.P.| + U \n (g)',
                                                'Conclusión'])
        self.table.horizontalHeader().setStyleSheet("::section{Background-color: #1c3048}")
        self.table.setSelectionBehavior(QTableWidget.SelectRows)
        self.table.setSelectionMode(QTableWidget.SingleSelection) 
        header = self.table.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.Stretch)
        self.table.verticalHeader().setVisible(False)
        self.table.setRowCount(3)

        self.gridLayout_4.addWidget(self.table)

        self.menuBtn.clicked.connect(self.AgrandarMenuLateral)

        self.RetornarBtn.clicked.connect(self.Regresar)
        self.RefrescarBtn.clicked.connect(self.Refresh)
        self.FilaBtn.clicked.connect(self.AddFila)
        self.GuardarBtn.clicked.connect(self.GuardarInfo)
        self.GenerarBtn.clicked.connect(self.Generar)
        self.AbrirBtn.clicked.connect(self.AbrirFile)
        self.MenosBtn.clicked.connect(self.DeleteFile)
        self.informacionBtn.clicked.connect(self.Information)

    def Information(self):
        Ui_Informacion().exec_()

    def DeleteFile(self):
        Row = self.table.rowCount()
        self.table.setRowCount(Row - 1)

    def AbrirFile(self):
        try:
            filename = QFileDialog.getOpenFileName(None, 'Abrir Archivos', filter='Lab Files (*.cmil)',
                                                   options=QFileDialog.Options())
            
            if filename[0]:
                for i in range(2, 9):
                    getattr(self, "lineEdit_{}".format(i)).setText("")

                for i in range(1, 3):
                    getattr(self, "comboBox_{}".format(i)).setItemText(0, "")
                    getattr(self, "comboBox_{}".format(i)).setCurrentIndex(0)
                
                for i in range(1, 2):
                    getattr(self, "spinBox_{}".format(i)).setValue(5)
                
                for i in range(1, 2):
                    getattr(self, "textEdit_{}".format(i)).setText("")
                
                self.table.clearContents()

                with open(filename[0], 'r', encoding='utf-8') as Info:

                    for i in range(2, 9):
                        val = str(Info.readline()).split('\n')                        
                        getattr(self, "lineEdit_{}".format(i)).setText(val[0])
                    
                    for i in range(1, 3):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "comboBox_{}".format(i)).setItemText(0, val[0])     
                        getattr(self, "comboBox_{}".format(i)).setCurrentIndex(0)    

                    for i in range(1, 2):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "spinBox_{}".format(i)).setValue(int(val[0])) 
                    
                    for i in range(1, 2):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "textEdit_{}".format(i)).setText(val[0])
                    
                    val = str(Info.readline()).split('\n')
                    Row = int(val[0])

                    self.table.setRowCount(Row)

                    for i in range(0, Row):
                        for j in range(0, 7):
                            val = str(Info.readline()).split('\n')
                            item = QTableWidgetItem(val[0])
                            self.table.setItem(i, j, item)
                   
        except Exception as e:
            print(e)

    def Generar(self):
        try:
            LinesEdits = np.zeros(7, dtype = object)
            for i in range(2, 9):
                LinesEdits[i-2] = getattr(self, "lineEdit_{}".format(i)).text()

            ComboBoxs = np.zeros(2, dtype = object)
            for i in range(1, 3):
                ComboBoxs[i-1] = getattr(self, "comboBox_{}".format(i)).currentText()
            
            SpinBoxs = np.zeros(1)
            for i in range(1, 2):
                SpinBoxs[i-1] = getattr(self, "spinBox_{}".format(i)).value()
            
            Row = self.table.rowCount()

            Indicacion = np.zeros(Row)
            Correccion = np.zeros(Row)
            Incertidumbre = np.zeros(Row)
            EMP = np.zeros(Row)

            for i in range(0, Row):
                item = self.table.item(i, 0)
                valor = float(str(item.text()).replace(',', '.'))
                Indicacion[i] = valor

                item = self.table.item(i, 1)
                valor = float(str(item.text()).replace(',', '.'))
                Correccion[i] = valor

                item = self.table.item(i, 2)
                valor = float(str(item.text()).replace(',', '.'))
                Incertidumbre[i] = valor

                item = self.table.item(i, 3)
                valor = float(str(item.text()).replace(',', '.'))
                EMP[i] = valor

            #------- Aplicar Cálculo -------#

            EMP_U_Menos = np.zeros(Row)
            EMP_U_Mas = np.zeros(Row)
            Mensaje = np.zeros(Row, dtype = object)

            for i in range(0, Row):
                EMP_U_Menos[i], EMP_U_Mas[i], Mensaje[i] = Ui_Main.Calculo(self, EMP[i], Incertidumbre[i], Correccion[i])

            for i in range(0, Row):
                self.table.setItem(i, 4, QTableWidgetItem(str(round(EMP_U_Menos[i], 5)).replace('.', ',')))
                self.table.setItem(i, 5, QTableWidgetItem(str(round(EMP_U_Mas[i], 5)).replace('.', ',')))
                self.table.setItem(i, 6, QTableWidgetItem(Mensaje[i]))

            #---------- Conclusiones finales ----------#

            Decision = True
            for i in range(0, Row):
                if (Mensaje[i] != "Aceptable"):
                    Decision = False
            
            if(Decision == True):
                Result = "Las Pesas con "+self.comboBox_1.currentText()+" N° "+self.lineEdit_6.text()+ " de " + self.lineEdit_7.text() + " cumple con las especificaciones técnicas."
                self.textEdit_1.setText(Result)
            else:
                Result = "Las Pesas con "+self.comboBox_1.currentText()+" N° "+self.lineEdit_6.text()+ " de " + self.lineEdit_7.text() + " no cumple con todas las especificaciones técnicas."
                self.textEdit_1.setText(Result)

            Fecha = str(datetime.date.today())

            Valor_1 = self.MakePesas(LinesEdits, ComboBoxs, SpinBoxs, Indicacion, Correccion,
                                     Incertidumbre, EMP, EMP_U_Menos, EMP_U_Mas, Mensaje, Result,
                                     Fecha)
            
            Valor_2 = self.GuardarFile()

            if (Valor_1 == True and Valor_2 == True):
                mesa="Correcto!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Information)
                msg.setText('Documento generado satisfactoriamente')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()
            
            elif (Valor_1 == True and Valor_2 == False):
                mesa="Revisar Información!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Warning)
                msg.setText('Se generó el documento pero no se guardó el archivo')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()
            
            elif (Valor_2 == True and Valor_1 == False):
                mesa="Revisar Información!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Warning)
                msg.setText('No se generó el documento pero sí se guardó el archivo.')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()
            
            else:
                mesa="Error!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Critical)
                msg.setText('Revisar toda la información suministrada')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()

        except Exception as e:
            print(e)
    
    def MakePesas(self, LinesEdits, ComboBoxs, SpinBoxs, Indicacion, Correccion,
                  Incertidumbre, EMP, EMP_U_Menos, EMP_U_Mas, Mensaje, Result,
                  Fecha):
        try:
            registerFont (TTFont ('Arial', 'ARIAL.ttf'))
            registerFont (TTFont ('Book', 'BOOKOSB.ttf'))
            registerFont (TTFont ('Arial-Bold', 'ARIALBd.ttf'))

            canv = PMCanvas (10,10)            
            fileName = self.ruta + "\\Pesas\\Documentos\\Confirmacion metrologica-" + ComboBoxs[0] + "-" + LinesEdits[0] + ".pdf"
            c = canvas.Canvas(fileName, pagesize=A4)
            p = ParagraphStyle(fileName)
            p.textColor = 'black'
            p.alignment = TA_JUSTIFY

            c.drawImage(self.ruta + "\\Base\\header_pesas.png", 20, 760, 550, 50)

            tamle=int(9)
            c.setFont ('Arial-Bold', tamle)

            c.drawString(405, 740, "Fecha: ")
            c.drawString(450, 740, Fecha) 
            c.drawString(50, 710, "1.- Equipo / Instrumento de Medición ") 
            c.drawString(350, 710, "Calibración vigente: ") 

            c.setFont ('Arial', tamle)
            c.drawString(60, 695, "Cód. Identificación: ")
            c.drawString(160, 695, LinesEdits[0]) 
            c.drawString(60, 680, "Equipo / Instrumento: ")
            c.drawString(160, 680, LinesEdits[1]) 
            c.drawString(60, 665, "Alcance de Indicación: ")
            c.drawString(160, 665, LinesEdits[2]) 
            c.drawString(60, 650, "Resolución: ")
            c.drawString(160, 650, LinesEdits[3]) 

            c.drawString(350, 695, "Documento: ")
            c.drawString(450, 695, ComboBoxs[0])
            c.drawString(350, 680, "Número: ")
            c.drawString(450, 680, LinesEdits[4])
            c.drawString(350, 665, "Institución: ")
            c.drawString(450, 665, LinesEdits[5])
            c.drawString(350, 650, "Fecha de calibración: ")
            c.drawString(450, 650, LinesEdits[6])
            
            c.setFont ('Arial-Bold', tamle)
            c.drawString(50, 630, "2.- Evaluación de Resultados")

            Titulo = [['Indicación \n ( kg )', 'Corrección \n ( g )', 'Incert. \n ( g )', 'E.M.P. \n ( ±g )']]

            table = Table(Titulo, [55, 55, 55, 55])
            table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                    ]))
            table.wrapOn(c, 10, 140)
            table.drawOn(c, 60, 590)

            Titulo = [['| E.M.P. | - U \n ( g )', '| E.M.P. | + U \n ( g )', 'Confirmación \n Metrológica']]

            table = Table(Titulo, [75, 75, 75])
            table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                    ]))
            table.wrapOn(c, 10, 140)
            table.drawOn(c, 340, 590)
            
            List_Indicacion = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in Indicacion.tolist()]
            List_Correccion = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in Correccion.tolist()]
            List_Incertidumbre = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in Incertidumbre.tolist()]
            List_EMP = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in EMP.tolist()]
            List_EMP_U_Menos = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in EMP_U_Menos.tolist()]
            List_EMP_U_Mas = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in EMP_U_Mas.tolist()]
            List_Mensaje = [[x] for x in Mensaje.tolist()]


            Rows = self.table.rowCount()
            Columns = self.table.columnCount()

            if (Rows < 41):
                Rot = Rows
                List_Indic = List_Indicacion
                List_Correc = List_Correccion
                List_Incert = List_Incertidumbre
                List_E = List_EMP
                List_EMP_U_Men = List_EMP_U_Menos
                List_EMP_U_Ma = List_EMP_U_Mas
                List_Mensa = List_Mensaje

            else:
                Rot = 40
                List_Indic = List_Indicacion[0:40]
                List_Correc = List_Correccion[0:40]
                List_Incert = List_Incertidumbre[0:40]
                List_E = List_EMP[0:40]
                List_EMP_U_Men = List_EMP_U_Menos[0:40]
                List_EMP_U_Ma = List_EMP_U_Mas[0:40]
                List_Mensa = List_Mensaje[0:40]            

            table = Table(List_Indic, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 60, 551 - (Rot - 3)*13)


            table = Table(List_Correc, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 115, 551 - (Rot - 3)*13) 


            table = Table(List_Incert, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 170, 551 - (Rot - 3)*13) 



            table = Table(List_E, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 225, 551 - (Rot - 3)*13) 


            table = Table(List_EMP_U_Men, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 340, 551 - (Rot - 3)*13) 


            table = Table(List_EMP_U_Ma, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 415, 551 - (Rot - 3)*13)


            table = Table(List_Mensa, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 490, 551 - (Rot - 3)*13)
            
            ux = 551 - (Rot - 3)*13

            if (Rows < 41):
                pass                
            else:
                c.showPage()
                if (Rows < 88):
                    Rot = len(List_Indicacion) - 40                    
                    List_Indic = List_Indicacion[40:len(List_Indicacion)]
                    List_Correc = List_Correccion[40:len(List_Indicacion)]
                    List_Incert = List_Incertidumbre[40:len(List_Indicacion)]
                    List_E = List_EMP[40:len(List_Indicacion)]
                    List_EMP_U_Men = List_EMP_U_Menos[40:len(List_Indicacion)]
                    List_EMP_U_Ma = List_EMP_U_Mas[40:len(List_Indicacion)]
                    List_Mensa = List_Mensaje[40:len(List_Indicacion)]
                
                else:
                    Rot = 87 - 40                    
                    List_Indic = List_Indicacion[40:87]
                    List_Correc = List_Correccion[40:87]
                    List_Incert = List_Incertidumbre[40:87]
                    List_E = List_EMP[40:87]
                    List_EMP_U_Men = List_EMP_U_Menos[40:87]
                    List_EMP_U_Ma = List_EMP_U_Mas[40:87]
                    List_Mensa = List_Mensaje[40:87]

                Titulo = [['Indicación \n ( kg )', 'Corrección \n ( g )', 'Incert. \n ( g )', 'E.M.P. \n ( ±g )']]

                table = Table(Titulo, [55, 55, 55, 55])
                table.setStyle(TableStyle([    
                        ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                        ('FONTSIZE',(0,0),(-1,-1),9),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                        ]))
                table.wrapOn(c, 10, 140)
                table.drawOn(c, 60, 730)

                Titulo = [['| E.M.P. | - U \n ( g )', '| E.M.P. | + U \n ( g )', 'Confirmación \n Metrológica']]

                table = Table(Titulo, [75, 75, 75])
                table.setStyle(TableStyle([    
                        ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                        ('FONTSIZE',(0,0),(-1,-1),9),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                        ]))
                table.wrapOn(c, 10, 140)
                table.drawOn(c, 340, 730)

                table = Table(List_Indic, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 60, 691 - (Rot - 3)*13)


                table = Table(List_Correc, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 115, 691 - (Rot - 3)*13) 


                table = Table(List_Incert, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 170, 691 - (Rot - 3)*13) 



                table = Table(List_E, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 225, 691 - (Rot - 3)*13) 


                table = Table(List_EMP_U_Men, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 340, 691 - (Rot - 3)*13) 


                table = Table(List_EMP_U_Ma, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 415, 691 - (Rot - 3)*13)


                table = Table(List_Mensa, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 490, 691 - (Rot - 3)*13)

                ux = 691 - (Rot - 3)*13

            if (Rows < 88):
                pass                
            else:
                c.showPage()
                if (Rows < 135):
                    Rot = len(List_Indicacion) - 87                    
                    List_Indic = List_Indicacion[87:len(List_Indicacion)]
                    List_Correc = List_Correccion[87:len(List_Indicacion)]
                    List_Incert = List_Incertidumbre[87:len(List_Indicacion)]
                    List_E = List_EMP[87:len(List_Indicacion)]
                    List_EMP_U_Men = List_EMP_U_Menos[87:len(List_Indicacion)]
                    List_EMP_U_Ma = List_EMP_U_Mas[87:len(List_Indicacion)]
                    List_Mensa = List_Mensaje[87:len(List_Indicacion)]
                
                else:
                    Rot = 134 - 87                    
                    List_Indic = List_Indicacion[87:134]
                    List_Correc = List_Correccion[87:134]
                    List_Incert = List_Incertidumbre[87:134]
                    List_E = List_EMP[87:134]
                    List_EMP_U_Men = List_EMP_U_Menos[87:134]
                    List_EMP_U_Ma = List_EMP_U_Mas[87:134]
                    List_Mensa = List_Mensaje[87:134]

                Titulo = [['Indicación \n ( kg )', 'Corrección \n ( g )', 'Incert. \n ( g )', 'E.M.P. \n ( ±g )']]

                table = Table(Titulo, [55, 55, 55, 55])
                table.setStyle(TableStyle([    
                        ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                        ('FONTSIZE',(0,0),(-1,-1),9),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                        ]))
                table.wrapOn(c, 10, 140)
                table.drawOn(c, 60, 730)

                Titulo = [['| E.M.P. | - U \n ( g )', '| E.M.P. | + U \n ( g )', 'Confirmación \n Metrológica']]

                table = Table(Titulo, [75, 75, 75])
                table.setStyle(TableStyle([    
                        ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                        ('FONTSIZE',(0,0),(-1,-1),9),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                        ]))
                table.wrapOn(c, 10, 140)
                table.drawOn(c, 340, 730)

                table = Table(List_Indic, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 60, 691 - (Rot - 3)*13)


                table = Table(List_Correc, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 115, 691 - (Rot - 3)*13) 


                table = Table(List_Incert, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 170, 691 - (Rot - 3)*13) 



                table = Table(List_E, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 225, 691 - (Rot - 3)*13) 


                table = Table(List_EMP_U_Men, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 340, 691 - (Rot - 3)*13) 


                table = Table(List_EMP_U_Ma, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 415, 691 - (Rot - 3)*13)


                table = Table(List_Mensa, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 490, 691 - (Rot - 3)*13)

                ux = 691 - (Rot - 3)*13
            
            if (Rows < 135):
                pass                
            else:
                c.showPage()
                if (Rows < 182):
                    Rot = len(List_Indicacion) - 134                    
                    List_Indic = List_Indicacion[134:len(List_Indicacion)]
                    List_Correc = List_Correccion[134:len(List_Indicacion)]
                    List_Incert = List_Incertidumbre[134:len(List_Indicacion)]
                    List_E = List_EMP[134:len(List_Indicacion)]
                    List_EMP_U_Men = List_EMP_U_Menos[134:len(List_Indicacion)]
                    List_EMP_U_Ma = List_EMP_U_Mas[134:len(List_Indicacion)]
                    List_Mensa = List_Mensaje[134:len(List_Indicacion)]
                
                else:
                    Rot = 181 - 134                    
                    List_Indic = List_Indicacion[134:181]
                    List_Correc = List_Correccion[134:181]
                    List_Incert = List_Incertidumbre[134:181]
                    List_E = List_EMP[134:181]
                    List_EMP_U_Men = List_EMP_U_Menos[134:181]
                    List_EMP_U_Ma = List_EMP_U_Mas[134:181]
                    List_Mensa = List_Mensaje[134:181]

                Titulo = [['Indicación \n ( kg )', 'Corrección \n ( g )', 'Incert. \n ( g )', 'E.M.P. \n ( ±g )']]

                table = Table(Titulo, [55, 55, 55, 55])
                table.setStyle(TableStyle([    
                        ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                        ('FONTSIZE',(0,0),(-1,-1),9),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                        ]))
                table.wrapOn(c, 10, 140)
                table.drawOn(c, 60, 730)

                Titulo = [['| E.M.P. | - U \n ( g )', '| E.M.P. | + U \n ( g )', 'Confirmación \n Metrológica']]

                table = Table(Titulo, [75, 75, 75])
                table.setStyle(TableStyle([    
                        ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                        ('FONTSIZE',(0,0),(-1,-1),9),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                        ]))
                table.wrapOn(c, 10, 140)
                table.drawOn(c, 340, 730)

                table = Table(List_Indic, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 60, 691 - (Rot - 3)*13)


                table = Table(List_Correc, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 115, 691 - (Rot - 3)*13) 


                table = Table(List_Incert, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 170, 691 - (Rot - 3)*13) 



                table = Table(List_E, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 225, 691 - (Rot - 3)*13) 


                table = Table(List_EMP_U_Men, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 340, 691 - (Rot - 3)*13) 


                table = Table(List_EMP_U_Ma, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 415, 691 - (Rot - 3)*13)


                table = Table(List_Mensa, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 490, 691 - (Rot - 3)*13)

                ux = 691 - (Rot - 3)*13
            

            if (ux - 20 > 200):
                c.setFont ('Arial-Bold', tamle)
                c.drawString(50, ux - 20, "3.- Criterio de Evaluación y Aceptación. ")

                data = [                
                        ['Criterio de Evaluación', 'Criterio de Aceptación'],
                        ['| E |  ≤  | E.M.P. | – U', 'Aceptable'],
                        ['| E.M.P. | – U  < | E |  ≤  | E.M.P. | + U', 'Indeterminado'],
                        ['| E.M.P. | + U  < | E |', 'No Aceptable'],
                    ]

                table = Table(data, [170, 150])
                table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('TEXTCOLOR',(0,-2),(-1,-1),colors.blue),
                    ('TEXTCOLOR',(0,-1),(-1,-1),colors.red),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'), #(0,-1),(-1,-1)
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER') #(0,0), (-1,-1)
                    ]))
                
                table .wrapOn(c, 10, 140)
                table .drawOn(c, 60, ux - 105)

                c.setFont ('Arial', tamle)

                c.drawString(400, ux - 105 + 60, "Donde: ")
                c.drawString(400, ux - 105 + 40, "E    :  Error / Corrección / Desviación")
                c.drawString(400, ux - 105 + 20, "E.M.P:  Error Máximo Permitido")
                c.drawString(400, ux - 105, "U    :  Incertidumbre Expandida")

            else:
                c.showPage()

                ux = 780
                c.setFont ('Arial-Bold', tamle)
                c.drawString(50, ux - 20, "3.- Criterio de Evaluación y Aceptación. ")

                data = [                
                        ['Criterio de Evaluación', 'Criterio de Aceptación'],
                        ['| E |  ≤  | E.M.P. | – U', 'Aceptable'],
                        ['| E.M.P. | – U  < | E |  ≤  | E.M.P. | + U', 'Indeterminado'],
                        ['| E.M.P. | + U  < | E |', 'No Aceptable'],
                    ]

                table = Table(data, [170, 150])
                table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('TEXTCOLOR',(0,-2),(-1,-1),colors.blue),
                    ('TEXTCOLOR',(0,-1),(-1,-1),colors.red),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'), #(0,-1),(-1,-1)
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER') #(0,0), (-1,-1)
                    ]))
                
                table .wrapOn(c, 10, 140)
                table .drawOn(c, 60, ux - 105)

                c.setFont ('Arial', tamle)

                c.drawString(400, ux - 105 + 60, "Donde: ")
                c.drawString(400, ux - 105 + 40, "E    :  Error / Corrección / Desviación")
                c.drawString(400, ux - 105 + 20, "E.M.P:  Error Máximo Permitido")
                c.drawString(400, ux - 105, "U    :  Incertidumbre Expandida")

            uy = ux - 105
                        
            Ui_Main.Grafica(self, Indicacion, Correccion, EMP_U_Mas,
                            EMP_U_Menos, EMP, int(SpinBoxs[0]), 
                            "Pesas " + self.lineEdit_2.text(), "P")

            if (uy > 300):
                c.drawImage('ConfirmacionP.png', 20, uy - 245, 570, 240)
            
            else:
                c.showPage()
                uy = 780
                c.drawImage('ConfirmacionP.png', 20, uy - 245, 570, 240)

            uz = uy - 245
            
            
            p = ParagraphStyle(fileName)
            p.textColor = 'black'
            p.alignment = TA_JUSTIFY
            p.fontName ='Arial-Bold'
            p.fontSize = 8
            p.leading = 11.2

            para = Paragraph(self.textEdit_1.toPlainText(), p)
            para.wrapOn(c, 450, 200)
            N_reglon_1 = para.height / 11.2

            try:
                if (uz > 170):            
                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(50, uz - 20, "4.- Conclusión:")
                    u1 = uz - 40 - int(11*(N_reglon_1-1))
                    para.drawOn(c, 60, u1)

                    c.setFont ('Arial', tamle)
                    c.drawString(250, u1 - 50, "Supervisado por:")
                    c.line(330, u1 - 60, 480, u1 - 60)

                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(350, u1 - 70, ComboBoxs[1])

                    c.drawImage(self.ruta + "\\Base\\" + ComboBoxs[1] + ".png", 340, u1 - 50, 140, 50)
                    
                else:
                    c.showPage()
                    uz = 780
                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(50, uz - 20, "4.- Conclusión:")
                    u1 = uz - 40 - int(11*(N_reglon_1-1))
                    para.drawOn(c, 60, u1)

                    c.setFont ('Arial', tamle)
                    c.drawString(250, u1 - 50, "Supervisado por:")
                    c.line(330, u1 - 60, 480, u1 - 60)

                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(350, u1 - 70, ComboBoxs[1])

                    c.drawImage(self.ruta + "\\Base\\" + ComboBoxs[1] + ".png", 340, u1 - 50, 140, 50)

            except:
                mensaje = "Falta Firma"
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setText("No existe la firma del supervisor")
                msg.setInformativeText(mensaje)
                msg.setStyleSheet("""background-color:#12abc4; color: white;""")
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()

            c.save()
            
            Info = True

        except Exception as e:
            Info = False
            print(e)
        
        return Info
    
    def GuardarInfo(self):

        Info = self.GuardarFile()

        if Info == True:
            mesa="Correcto!"
            msg = QMessageBox()
            msg.setStyleSheet("""background-color: #12abc4;
                                    color: white;            
            """)
            msg.setIcon(QMessageBox.Information)
            msg.setText('Archivo guardado satisfactoriamente')
            msg.setInformativeText(mesa)
            msg.setWindowTitle("METROIL S.A.C.")
            msg.exec_()
        else:
            mesa="Error!"
            msg = QMessageBox()
            msg.setStyleSheet("""background-color: #12abc4;
                                    color: white;            
            """)
            msg.setIcon(QMessageBox.Critical)
            msg.setText('Colocar código del instrumento')
            msg.setInformativeText(mesa)
            msg.setWindowTitle("METROIL S.A.C.")
            msg.exec_()

    def GuardarFile(self):
        try:
            Ano = str(datetime.date.today()).split("-")
            Ano = Ano[0]            
            
            nom1 = "Pesas"
            nom2 = self.lineEdit_2.text()
            nom3 = Ano

            if (nom1 != "" and nom2 != "" and nom3 != ""):
                self.NameG = nom1+"-"+nom2+"-"+nom3+".cmil"
                Info = open(self.ruta + "\\Pesas\\Archivos Files\\" + self.NameG, 'w',encoding='utf-8')

                for i in range(2, 9):
                    Info.write(str(getattr(self, "lineEdit_{}".format(i)).text()) + '\n')
                
                for i in range(1, 3):
                    Info.write(str(getattr(self, "comboBox_{}".format(i)).currentText()) + '\n')
                
                for i in range(1, 2):
                    Info.write(str(getattr(self, "spinBox_{}".format(i)).value()) + '\n')
                
                for i in range(1, 2):
                    Info.write(str(getattr(self, "textEdit_{}".format(i)).toPlainText()) + '\n')
                
                Row = self.table.rowCount()

                Info.write(str(Row)+ '\n')

                rowdata = []
                for row in range(self.table.rowCount()):                        
                    for column in range(self.table.columnCount()):
                        item = self.table.item(row, column)                        
                        if item is not None:
                            rowdata.append(item.text())
                        else:
                            rowdata.append('')
                
                for i in range(0, len(rowdata)):
                    Info.write(str(rowdata[i])+'\n')
                
                Reenviar = True

            else:
                Reenviar = False

            return Reenviar

        except Exception as e:
            print(e)

    def Refresh(self):
        self.hide()
        self.ui = Ui_Main_3()
        self.ui.show()

    def Regresar(self):
        self.close()
        self.ui = Start()
        self.ui.show()

    def AddFila(self):
        Row = self.table.rowCount()

        self.table.setRowCount(Row + 1)        

    def AgrandarMenuLateral(self):
        if True:
            width = self.leftMenuContenedor.width()
            norm = 65
            if width == 65:
                exten = 200
            else:
                exten = norm

            self.ani = QPropertyAnimation(self.leftMenuContenedor, b'minimumWidth')
            self.ani.setDuration(300)
            self.ani.setStartValue(width)
            self.ani.setEndValue(exten)
            self.ani.setEasingCurve(QtCore.QEasingCurve.InOutQuart)
            self.ani.start()

class Ui_Main_4(QMainWindow):
    def __init__(self):        
        super(Ui_Main_4, self).__init__()
        uic.loadUi("Manometros.ui", self)        

        self.setWindowIcon(QIcon('0.ico'))

        try:
            ruta_1 = open("ruta.rut",'r', encoding='utf-8')
            ruta = ruta_1.readline()
            self.ruta = ruta
            self.ruta = r'{}'.format(self.ruta)
            ruta_1.close()
        except Exception as e:
            print(e)

        unid = open(self.ruta+"\\Base\\Supervisores.til",'r',encoding='utf-8')        
        unida = unid.readlines()            
        unid.close()

        for i in range(0, len(unida)):
            valunida = unida[i].split('\n')                
            self.comboBox_2.addItem(valunida[0])
        self.comboBox_2.setCurrentIndex(1)

        self.table = QTableWidget(0, 7)
        self.table.setHorizontalHeaderLabels(['Indicación \n (Unidad)',
                                                'Corrección \n (Unidad)',
                                                'Incertidumbre \n (Unidad)',
                                                'E.M.P. \n ( ± Unidad)',
                                                '|E.M.P.| - U \n (Unidad)',
                                                '|E.M.P.| + U \n (Unidad)',
                                                'Conclusión'])
        self.table.horizontalHeader().setStyleSheet("::section{Background-color: #1c3048}")
        self.table.setSelectionBehavior(QTableWidget.SelectRows)
        self.table.setSelectionMode(QTableWidget.SingleSelection) 
        header = self.table.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.Stretch)
        self.table.verticalHeader().setVisible(False)
        self.table.setRowCount(3)

        self.gridLayout_4.addWidget(self.table)

        self.menuBtn.clicked.connect(self.AgrandarMenuLateral)

        self.RetornarBtn.clicked.connect(self.Regresar)
        self.RefrescarBtn.clicked.connect(self.Refresh)
        self.FilaBtn.clicked.connect(self.AddFila)
        self.GuardarBtn.clicked.connect(self.GuardarInfo)
        self.GenerarBtn.clicked.connect(self.Generar)
        self.AbrirBtn.clicked.connect(self.AbrirFile)
        self.MenosBtn.clicked.connect(self.DeleteFile)
        self.informacionBtn.clicked.connect(self.Information)

        self.InsertBtn.clicked.connect(self.ReadPDF)
    
    def ReadPDF(self):
        try:
            filename = QFileDialog.getOpenFileNames(None, 'Abrir Archivo', filter='Archivos de PDF (*.pdf)')
            
            filename = filename[0]
            filename = filename[0]

            Indicacion, Correccion, Incertidumbre, Unidad = self.PDF(filename)
            row = len(Indicacion)

            self.table.clear()

            self.table.setHorizontalHeaderLabels(['Indicación \n ( ' + Unidad + ' )', 'Corrección \n ( ' + Unidad + ' )', 'Incertidumbre \n ( ' + Unidad + ' )', 
                                                'E.M.P. \n ( ±' + Unidad + ' )',
                                                '|E.M.P.| - U \n ( ' + Unidad + ' )',
                                                '|E.M.P.| + U \n ( ' + Unidad + ' )',
                                                'Conclusión'])

            self.table.horizontalHeader().setStyleSheet("::section{Background-color: #1c3048}")
            self.table.setSelectionBehavior(QTableWidget.SelectRows)
            self.table.setSelectionMode(QTableWidget.SingleSelection) 
            header = self.table.horizontalHeader()
            header.setSectionResizeMode(QHeaderView.Stretch)
            self.table.verticalHeader().setVisible(False)
            self.table.setRowCount(row)               

            for i in range(0, row):
                self.table.setItem(i, 0, QTableWidgetItem(str(Indicacion[i])))
                self.table.setItem(i, 1, QTableWidgetItem(str(Correccion[i])))
                self.table.setItem(i, 2, QTableWidgetItem(str(Incertidumbre[i])))   

            self.comboBox_3.setItemText(0, Unidad)    
            self.comboBox_3.setCurrentIndex(0) 

        
        except Exception as e:
            print(e)

    def PDF(self, name):
        archivo_pdf = name
        numero_pagina = 2

        #----------------- Read the pdf file -----------------#
        df = tabula.read_pdf(archivo_pdf, pages = numero_pagina)
        DataFrame = df[0] #----- Make de DataFrame
        Longitud = len(DataFrame.iloc[:,0])

        Indicacion = np.zeros(11)
        Correccion = np.zeros(11)
        Incertidumbre = np.zeros(11)

        if (Longitud == 14):
            pass

        else:
            DataFrame = df[1]

        Unidad = DataFrame.iloc[2,1].split('( ')
        Unidad = Unidad[1].split(' )')
        Unidad = Unidad[0]

        Ind = DataFrame.iloc[:, 1]
        Err = DataFrame.iloc[:, 3]
        Inc = DataFrame.iloc[:, 4]

        for i in range(0, 11):
            Value = Ind[i + 3].replace(',','.')
            Indicacion[i] = float(Value.replace(' ','')) + 0

            Value = Err[i + 3].replace(',','.')
            Correccion[i] = -1*float(Value.replace(' ','')) + 0

            Value = Inc[i + 3].replace(',','.')
            Incertidumbre[i] = float(Value.replace(' ','')) + 0

            
        return Indicacion, Correccion, Incertidumbre, Unidad

    def Generar(self):
        try:
            LinesEdits = np.zeros(7, dtype = object)
            for i in range(2, 9):
                LinesEdits[i-2] = getattr(self, "lineEdit_{}".format(i)).text()

            ComboBoxs = np.zeros(3, dtype = object)
            for i in range(1, 4):
                ComboBoxs[i-1] = getattr(self, "comboBox_{}".format(i)).currentText()
            
            SpinBoxs = np.zeros(1)
            for i in range(1, 2):
                SpinBoxs[i-1] = getattr(self, "spinBox_{}".format(i)).value()
            
            Row = self.table.rowCount()

            Indicacion = np.zeros(Row)
            Correccion = np.zeros(Row)
            Incertidumbre = np.zeros(Row)
            EMP = np.zeros(Row)

            for i in range(0, Row):
                item = self.table.item(i, 0)
                valor = float(str(item.text()).replace(',', '.'))
                Indicacion[i] = valor

                item = self.table.item(i, 1)
                valor = float(str(item.text()).replace(',', '.'))
                Correccion[i] = valor

                item = self.table.item(i, 2)
                valor = float(str(item.text()).replace(',', '.'))
                Incertidumbre[i] = valor

                item = self.table.item(i, 3)
                valor = float(str(item.text()).replace(',', '.'))
                EMP[i] = valor

            #------- Aplicar Cálculo -------#

            EMP_U_Menos = np.zeros(Row)
            EMP_U_Mas = np.zeros(Row)
            Mensaje = np.zeros(Row, dtype = object)

            for i in range(0, Row):
                EMP_U_Menos[i], EMP_U_Mas[i], Mensaje[i] = Ui_Main.Calculo(self, EMP[i], Incertidumbre[i], Correccion[i])

            for i in range(0, Row):
                self.table.setItem(i, 4, QTableWidgetItem(str(round(EMP_U_Menos[i], 5)).replace('.', ',')))
                self.table.setItem(i, 5, QTableWidgetItem(str(round(EMP_U_Mas[i], 5)).replace('.', ',')))
                self.table.setItem(i, 6, QTableWidgetItem(Mensaje[i]))

            #---------- Conclusiones finales ----------#

            Decision = True
            for i in range(0, Row):
                if (Mensaje[i] != "Aceptable"):
                    Decision = False
            
            if(Decision == True):
                Result = "El manómetro con "+self.comboBox_1.currentText()+" N° "+self.lineEdit_6.text()+ " de " + self.lineEdit_7.text() + " cumple con las especificaciones técnicas."
                self.textEdit_1.setText(Result)
            else:
                Result = "El manómetro con "+self.comboBox_1.currentText()+" N° "+self.lineEdit_6.text()+ " de " + self.lineEdit_7.text() + " no cumple con todas las especificaciones técnicas."
                self.textEdit_1.setText(Result)

            Fecha = str(datetime.date.today())

            Valor_1 = self.MakeManometro(LinesEdits, ComboBoxs, SpinBoxs, Indicacion, Correccion,
                                        Incertidumbre, EMP, EMP_U_Menos, EMP_U_Mas, Mensaje, Result,
                                        Fecha)
            
            Valor_2 = self.GuardarFile()

            if (Valor_1 == True and Valor_2 == True):
                mesa="Correcto!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Information)
                msg.setText('Documento generado satisfactoriamente')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()
            
            elif (Valor_1 == True and Valor_2 == False):
                mesa="Revisar Información!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Warning)
                msg.setText('Se generó el documento pero no se guardó el archivo')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()
            
            elif (Valor_2 == True and Valor_1 == False):
                mesa="Revisar Información!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Warning)
                msg.setText('No se generó el documento pero sí se guardó el archivo.')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()
            
            else:
                mesa="Error!"
                msg = QMessageBox()
                msg.setStyleSheet("""background-color: #12abc4;
                                        color: white;            
                """)
                msg.setIcon(QMessageBox.Critical)
                msg.setText('Revisar toda la información suministrada')
                msg.setInformativeText(mesa)
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()

        except Exception as e:
            print(e)

    def MakeManometro(self, LinesEdits, ComboBoxs, SpinBoxs, Indicacion, Correccion,
                    Incertidumbre, EMP, EMP_U_Menos, EMP_U_Mas, Mensaje, Result,
                    Fecha):
        try:
            registerFont (TTFont ('Arial', 'ARIAL.ttf'))
            registerFont (TTFont ('Book', 'BOOKOSB.ttf'))
            registerFont (TTFont ('Arial-Bold', 'ARIALBd.ttf'))

            canv = PMCanvas (10,10)            
            fileName = self.ruta + "\\Manometros\\Documentos\\Confirmacion metrologica-" + ComboBoxs[0] + "-" + LinesEdits[0] + ".pdf"
            c = canvas.Canvas(fileName, pagesize=A4)
            p = ParagraphStyle(fileName)
            p.textColor = 'black'
            p.alignment = TA_JUSTIFY

            c.drawImage(self.ruta + "\\Base\\header_manometros.png", 20, 760, 550, 50)

            tamle=int(9)
            c.setFont ('Arial-Bold', tamle)

            c.drawString(405, 740, "Fecha: ")
            c.drawString(450, 740, Fecha) 
            c.drawString(50, 710, "1.- Equipo / Instrumento de Medición ") 
            c.drawString(350, 710, "Calibración vigente: ") 

            c.setFont ('Arial', tamle)
            c.drawString(60, 695, "Cód. Identificación: ")
            c.drawString(160, 695, LinesEdits[0]) 
            c.drawString(60, 680, "Equipo / Instrumento: ")
            c.drawString(160, 680, LinesEdits[1]) 
            c.drawString(60, 665, "Alcance de Indicación: ")
            c.drawString(160, 665, LinesEdits[2]) 
            c.drawString(60, 650, "Resolución: ")
            c.drawString(160, 650, LinesEdits[3]) 

            c.drawString(350, 695, "Documento: ")
            c.drawString(450, 695, ComboBoxs[0])
            c.drawString(350, 680, "Número: ")
            c.drawString(450, 680, LinesEdits[4])
            c.drawString(350, 665, "Institución: ")
            c.drawString(450, 665, LinesEdits[5])
            c.drawString(350, 650, "Fecha de calibración: ")
            c.drawString(450, 650, LinesEdits[6])
            
            c.setFont ('Arial-Bold', tamle)
            c.drawString(50, 630, "2.- Evaluación de Resultados")

            Titulo = [['Indicación \n ( '+ComboBoxs[2] + ' )', 'Corrección \n ( '+ComboBoxs[2] + ' )', 'Incert. \n ( '+ComboBoxs[2] + ' )', 'E.M.P. \n ( ± '+ComboBoxs[2] + ' )']]

            table = Table(Titulo, [55, 55, 55, 55])
            table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                    ]))
            table.wrapOn(c, 10, 140)
            table.drawOn(c, 60, 590)

            Titulo = [['| E.M.P. | - U \n ( '+ComboBoxs[2] + ' )', '| E.M.P. | + U \n ( '+ComboBoxs[2] + ' )', 'Confirmación \n Metrológica']]

            table = Table(Titulo, [75, 75, 75])
            table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                    ]))
            table.wrapOn(c, 10, 140)
            table.drawOn(c, 340, 590)
            
            List_Indicacion = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in Indicacion.tolist()]
            List_Correccion = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in Correccion.tolist()]
            List_Incertidumbre = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in Incertidumbre.tolist()]
            List_EMP = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in EMP.tolist()]
            List_EMP_U_Menos = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in EMP_U_Menos.tolist()]
            List_EMP_U_Mas = [[Coma.Deci2(x, int(SpinBoxs[0]))] for x in EMP_U_Mas.tolist()]
            List_Mensaje = [[x] for x in Mensaje.tolist()]


            Rows = self.table.rowCount()
            Columns = self.table.columnCount()

            if (Rows < 41):
                Rot = Rows
                List_Indic = List_Indicacion
                List_Correc = List_Correccion
                List_Incert = List_Incertidumbre
                List_E = List_EMP
                List_EMP_U_Men = List_EMP_U_Menos
                List_EMP_U_Ma = List_EMP_U_Mas
                List_Mensa = List_Mensaje

            else:
                Rot = 40
                List_Indic = List_Indicacion[0:40]
                List_Correc = List_Correccion[0:40]
                List_Incert = List_Incertidumbre[0:40]
                List_E = List_EMP[0:40]
                List_EMP_U_Men = List_EMP_U_Menos[0:40]
                List_EMP_U_Ma = List_EMP_U_Mas[0:40]
                List_Mensa = List_Mensaje[0:40]            

            table = Table(List_Indic, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 60, 551 - (Rot - 3)*13)


            table = Table(List_Correc, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 115, 551 - (Rot - 3)*13) 


            table = Table(List_Incert, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 170, 551 - (Rot - 3)*13) 



            table = Table(List_E, [55])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 225, 551 - (Rot - 3)*13) 


            table = Table(List_EMP_U_Men, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 340, 551 - (Rot - 3)*13) 


            table = Table(List_EMP_U_Ma, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 415, 551 - (Rot - 3)*13)


            table = Table(List_Mensa, [75])
            table.setStyle(TableStyle([                        
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                    ('FONTNAME',(0,0),(-1,-1),'Arial'),
                    ('FONTSIZE',(0,0),(-1,-1), 8),
                    ('LEADING',(0,0),(-1,-1),7),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER')
                    ]))

            table.wrapOn(c, 10, 90)
            table.drawOn(c, 490, 551 - (Rot - 3)*13)
            
            ux = 551 - (Rot - 3)*13

            if (Rows < 41):
                pass                
            else:
                c.showPage()
                if (Rows < 88):
                    Rot = len(List_Indicacion) - 40                    
                    List_Indic = List_Indicacion[40:len(List_Indicacion)]
                    List_Correc = List_Correccion[40:len(List_Indicacion)]
                    List_Incert = List_Incertidumbre[40:len(List_Indicacion)]
                    List_E = List_EMP[40:len(List_Indicacion)]
                    List_EMP_U_Men = List_EMP_U_Menos[40:len(List_Indicacion)]
                    List_EMP_U_Ma = List_EMP_U_Mas[40:len(List_Indicacion)]
                    List_Mensa = List_Mensaje[40:len(List_Indicacion)]
                
                else:
                    Rot = 87 - 40                    
                    List_Indic = List_Indicacion[40:87]
                    List_Correc = List_Correccion[40:87]
                    List_Incert = List_Incertidumbre[40:87]
                    List_E = List_EMP[40:87]
                    List_EMP_U_Men = List_EMP_U_Menos[40:87]
                    List_EMP_U_Ma = List_EMP_U_Mas[40:87]
                    List_Mensa = List_Mensaje[40:87]

                Titulo = [['Indicación \n ( '+ComboBoxs[2] + ' )', 'Corrección \n ( '+ComboBoxs[2] + ' )', 'Incert. \n ( '+ComboBoxs[2] + ' )', 'E.M.P. \n ( ± '+ComboBoxs[2] + ' )']]

                table = Table(Titulo, [55, 55, 55, 55])
                table.setStyle(TableStyle([    
                        ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                        ('FONTSIZE',(0,0),(-1,-1),9),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                        ]))
                table.wrapOn(c, 10, 140)
                table.drawOn(c, 60, 730)

                Titulo = [['| E.M.P. | - U \n ( '+ComboBoxs[2] + ' )', '| E.M.P. | + U \n ( '+ComboBoxs[2] + ' )', 'Confirmación \n Metrológica']]

                table = Table(Titulo, [75, 75, 75])
                table.setStyle(TableStyle([    
                        ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                        ('FONTSIZE',(0,0),(-1,-1),9),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                        ]))
                table.wrapOn(c, 10, 140)
                table.drawOn(c, 340, 730)

                table = Table(List_Indic, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 60, 691 - (Rot - 3)*13)


                table = Table(List_Correc, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 115, 691 - (Rot - 3)*13) 


                table = Table(List_Incert, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 170, 691 - (Rot - 3)*13) 



                table = Table(List_E, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 225, 691 - (Rot - 3)*13) 


                table = Table(List_EMP_U_Men, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 340, 691 - (Rot - 3)*13) 


                table = Table(List_EMP_U_Ma, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 415, 691 - (Rot - 3)*13)


                table = Table(List_Mensa, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 490, 691 - (Rot - 3)*13)

                ux = 691 - (Rot - 3)*13

            if (Rows < 88):
                pass                
            else:
                c.showPage()
                if (Rows < 135):
                    Rot = len(List_Indicacion) - 87                    
                    List_Indic = List_Indicacion[87:len(List_Indicacion)]
                    List_Correc = List_Correccion[87:len(List_Indicacion)]
                    List_Incert = List_Incertidumbre[87:len(List_Indicacion)]
                    List_E = List_EMP[87:len(List_Indicacion)]
                    List_EMP_U_Men = List_EMP_U_Menos[87:len(List_Indicacion)]
                    List_EMP_U_Ma = List_EMP_U_Mas[87:len(List_Indicacion)]
                    List_Mensa = List_Mensaje[87:len(List_Indicacion)]
                
                else:
                    Rot = 134 - 87                    
                    List_Indic = List_Indicacion[87:134]
                    List_Correc = List_Correccion[87:134]
                    List_Incert = List_Incertidumbre[87:134]
                    List_E = List_EMP[87:134]
                    List_EMP_U_Men = List_EMP_U_Menos[87:134]
                    List_EMP_U_Ma = List_EMP_U_Mas[87:134]
                    List_Mensa = List_Mensaje[87:134]

                Titulo = [['Indicación \n ( '+ComboBoxs[2] + ' )', 'Corrección \n ( '+ComboBoxs[2] + ' )', 'Incert. \n ( '+ComboBoxs[2] + ' )', 'E.M.P. \n ( ± '+ComboBoxs[2] + ' )']]

                table = Table(Titulo, [55, 55, 55, 55])
                table.setStyle(TableStyle([    
                        ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                        ('FONTSIZE',(0,0),(-1,-1),9),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                        ]))
                table.wrapOn(c, 10, 140)
                table.drawOn(c, 60, 730)

                Titulo = [['| E.M.P. | - U \n ( '+ComboBoxs[2] + ' )', '| E.M.P. | + U \n ( '+ComboBoxs[2] + ' )', 'Confirmación \n Metrológica']]

                table = Table(Titulo, [75, 75, 75])
                table.setStyle(TableStyle([    
                        ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                        ('FONTSIZE',(0,0),(-1,-1),9),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                        ]))
                table.wrapOn(c, 10, 140)
                table.drawOn(c, 340, 730)

                table = Table(List_Indic, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 60, 691 - (Rot - 3)*13)


                table = Table(List_Correc, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 115, 691 - (Rot - 3)*13) 


                table = Table(List_Incert, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 170, 691 - (Rot - 3)*13) 



                table = Table(List_E, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 225, 691 - (Rot - 3)*13) 


                table = Table(List_EMP_U_Men, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 340, 691 - (Rot - 3)*13) 


                table = Table(List_EMP_U_Ma, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 415, 691 - (Rot - 3)*13)


                table = Table(List_Mensa, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 490, 691 - (Rot - 3)*13)

                ux = 691 - (Rot - 3)*13
            
            if (Rows < 135):
                pass                
            else:
                c.showPage()
                if (Rows < 182):
                    Rot = len(List_Indicacion) - 134                    
                    List_Indic = List_Indicacion[134:len(List_Indicacion)]
                    List_Correc = List_Correccion[134:len(List_Indicacion)]
                    List_Incert = List_Incertidumbre[134:len(List_Indicacion)]
                    List_E = List_EMP[134:len(List_Indicacion)]
                    List_EMP_U_Men = List_EMP_U_Menos[134:len(List_Indicacion)]
                    List_EMP_U_Ma = List_EMP_U_Mas[134:len(List_Indicacion)]
                    List_Mensa = List_Mensaje[134:len(List_Indicacion)]
                
                else:
                    Rot = 181 - 134                    
                    List_Indic = List_Indicacion[134:181]
                    List_Correc = List_Correccion[134:181]
                    List_Incert = List_Incertidumbre[134:181]
                    List_E = List_EMP[134:181]
                    List_EMP_U_Men = List_EMP_U_Menos[134:181]
                    List_EMP_U_Ma = List_EMP_U_Mas[134:181]
                    List_Mensa = List_Mensaje[134:181]

                Titulo = [['Indicación \n ( '+ComboBoxs[2] + ' )', 'Corrección \n ( '+ComboBoxs[2] + ' )', 'Incert. \n ( '+ComboBoxs[2] + ' )', 'E.M.P. \n ( ±'+ComboBoxs[2] + ' )']]

                table = Table(Titulo, [55, 55, 55, 55])
                table.setStyle(TableStyle([    
                        ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                        ('FONTSIZE',(0,0),(-1,-1),9),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                        ]))
                table.wrapOn(c, 10, 140)
                table.drawOn(c, 60, 730)

                Titulo = [['| E.M.P. | - U \n ( '+ComboBoxs[2] + ' )', '| E.M.P. | + U \n ( '+ComboBoxs[2] + ' )', 'Confirmación \n Metrológica']]

                table = Table(Titulo, [75, 75, 75])
                table.setStyle(TableStyle([    
                        ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                        ('FONTSIZE',(0,0),(-1,-1),9),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER'),
                        ]))
                table.wrapOn(c, 10, 140)
                table.drawOn(c, 340, 730)

                table = Table(List_Indic, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 60, 691 - (Rot - 3)*13)


                table = Table(List_Correc, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 115, 691 - (Rot - 3)*13) 


                table = Table(List_Incert, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 170, 691 - (Rot - 3)*13) 



                table = Table(List_E, [55])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 225, 691 - (Rot - 3)*13) 


                table = Table(List_EMP_U_Men, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 340, 691 - (Rot - 3)*13) 


                table = Table(List_EMP_U_Ma, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 415, 691 - (Rot - 3)*13)


                table = Table(List_Mensa, [75])
                table.setStyle(TableStyle([                        
                        ('BOX', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                        ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                        ('VALIGN',(0,0),(-1,-1),'MIDDLE'),
                        ('FONTNAME',(0,0),(-1,-1),'Arial'),
                        ('FONTSIZE',(0,0),(-1,-1), 8),
                        ('LEADING',(0,0),(-1,-1),7),
                        ('ALIGN', (0,0), (-1,-1), 'CENTER')
                        ]))

                table.wrapOn(c, 10, 90)
                table.drawOn(c, 490, 691 - (Rot - 3)*13)

                ux = 691 - (Rot - 3)*13
            

            if (ux - 20 > 200):
                c.setFont ('Arial-Bold', tamle)
                c.drawString(50, ux - 20, "3.- Criterio de Evaluación y Aceptación. ")

                data = [                
                        ['Criterio de Evaluación', 'Criterio de Aceptación'],
                        ['| E |  ≤  | E.M.P. | – U', 'Aceptable'],
                        ['| E.M.P. | – U  < | E |  ≤  | E.M.P. | + U', 'Indeterminado'],
                        ['| E.M.P. | + U  < | E |', 'No Aceptable'],
                    ]

                table = Table(data, [170, 150])
                table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('TEXTCOLOR',(0,-2),(-1,-1),colors.blue),
                    ('TEXTCOLOR',(0,-1),(-1,-1),colors.red),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'), #(0,-1),(-1,-1)
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER') #(0,0), (-1,-1)
                    ]))
                
                table .wrapOn(c, 10, 140)
                table .drawOn(c, 60, ux - 105)

                c.setFont ('Arial', tamle)

                c.drawString(400, ux - 105 + 60, "Donde: ")
                c.drawString(400, ux - 105 + 40, "E    :  Error / Corrección / Desviación")
                c.drawString(400, ux - 105 + 20, "E.M.P:  Error Máximo Permitido")
                c.drawString(400, ux - 105, "U    :  Incertidumbre Expandida")

            else:
                c.showPage()

                ux = 780
                c.setFont ('Arial-Bold', tamle)
                c.drawString(50, ux - 20, "3.- Criterio de Evaluación y Aceptación. ")

                data = [                
                        ['Criterio de Evaluación', 'Criterio de Aceptación'],
                        ['| E |  ≤  | E.M.P. | – U', 'Aceptable'],
                        ['| E.M.P. | – U  < | E |  ≤  | E.M.P. | + U', 'Indeterminado'],
                        ['| E.M.P. | + U  < | E |', 'No Aceptable'],
                    ]

                table = Table(data, [170, 150])
                table.setStyle(TableStyle([    
                    ('BACKGROUND', (0,0), (3,0), colors.lightgrey), #0,0 2,0
                    ('BOX', (0,0), (-1,-1),0.5, colors.black),
                    ('TEXTCOLOR',(0,-2),(-1,-1),colors.blue),
                    ('TEXTCOLOR',(0,-1),(-1,-1),colors.red),
                    ('LINEBEFORE', (0,0), (-1,-1),0.5, colors.black),
                    ('LINEABOVE', (0,0), (-1,-1),0.5, colors.black),
                    ('VALIGN',(0,0),(-1,-1),'MIDDLE'), #(0,-1),(-1,-1)
                    ('FONTNAME',(0,0),(-1,0),'Arial-Bold'),
                    ('FONTSIZE',(0,0),(-1,-1),9),
                    ('ALIGN', (0,0), (-1,-1), 'CENTER') #(0,0), (-1,-1)
                    ]))
                
                table .wrapOn(c, 10, 140)
                table .drawOn(c, 60, ux - 105)

                c.setFont ('Arial', tamle)

                c.drawString(400, ux - 105 + 60, "Donde: ")
                c.drawString(400, ux - 105 + 40, "E    :  Error / Corrección / Desviación")
                c.drawString(400, ux - 105 + 20, "E.M.P:  Error Máximo Permitido")
                c.drawString(400, ux - 105, "U    :  Incertidumbre Expandida")

            uy = ux - 105
                        
            Ui_Main.Grafica(self, Indicacion, Correccion, EMP_U_Mas,
                            EMP_U_Menos, EMP, int(SpinBoxs[0]), 
                            "Manómetro " + self.lineEdit_2.text(), "M " + ComboBoxs[2])

            if (uy > 300):
                c.drawImage('ConfirmacionM.png', 20, uy - 245, 570, 240)
            
            else:
                c.showPage()
                uy = 780
                c.drawImage('ConfirmacionM.png', 20, uy - 245, 570, 240)

            uz = uy - 245
            
            
            p = ParagraphStyle(fileName)
            p.textColor = 'black'
            p.alignment = TA_JUSTIFY
            p.fontName ='Arial-Bold'
            p.fontSize = 8
            p.leading = 11.2

            para = Paragraph(self.textEdit_1.toPlainText(), p)
            para.wrapOn(c, 450, 200)
            N_reglon_1 = para.height / 11.2

            try:
                if (uz > 170):            
                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(50, uz - 20, "4.- Conclusión:")
                    u1 = uz - 40 - int(11*(N_reglon_1-1))
                    para.drawOn(c, 60, u1)

                    c.setFont ('Arial', tamle)
                    c.drawString(250, u1 - 50, "Supervisado por:")
                    c.line(330, u1 - 60, 480, u1 - 60)

                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(350, u1 - 70, ComboBoxs[1])

                    c.drawImage(self.ruta + "\\Base\\" + ComboBoxs[1] + ".png", 340, u1 - 50, 140, 50)
                    
                else:
                    c.showPage()
                    uz = 780
                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(50, uz - 20, "4.- Conclusión:")
                    u1 = uz - 40 - int(11*(N_reglon_1-1))
                    para.drawOn(c, 60, u1)

                    c.setFont ('Arial', tamle)
                    c.drawString(250, u1 - 50, "Supervisado por:")
                    c.line(330, u1 - 60, 480, u1 - 60)

                    c.setFont ('Arial-Bold', tamle)
                    c.drawString(350, u1 - 70, ComboBoxs[1])

                    c.drawImage(self.ruta + "\\Base\\" + ComboBoxs[1] + ".png", 340, u1 - 50, 140, 50)

            except:
                mensaje = "Falta Firma"
                msg = QMessageBox()
                msg.setIcon(QMessageBox.Warning)
                msg.setText("No existe la firma del supervisor")
                msg.setInformativeText(mensaje)
                msg.setStyleSheet("""background-color:#12abc4; color: white;""")
                msg.setWindowTitle("METROIL S.A.C.")
                msg.exec_()

            c.save()
            
            Info = True

        except Exception as e:
            Info = False
            print(e)
        
        return Info
        
    def AbrirFile(self):
        try:
            filename = QFileDialog.getOpenFileName(None, 'Abrir Archivos', filter='Lab Files (*.cmil)',
                                                   options=QFileDialog.Options())
            
            if filename[0]:
                for i in range(2, 9):
                    getattr(self, "lineEdit_{}".format(i)).setText("")

                for i in range(1, 4):
                    getattr(self, "comboBox_{}".format(i)).setItemText(0, "")
                    getattr(self, "comboBox_{}".format(i)).setCurrentIndex(0)
                
                for i in range(1, 2):
                    getattr(self, "spinBox_{}".format(i)).setValue(5)
                
                for i in range(1, 2):
                    getattr(self, "textEdit_{}".format(i)).setText("")
                
                self.table.clearContents()

                with open(filename[0], 'r', encoding='utf-8') as Info:

                    for i in range(2, 9):
                        val = str(Info.readline()).split('\n')                        
                        getattr(self, "lineEdit_{}".format(i)).setText(val[0])
                    
                    for i in range(1, 4):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "comboBox_{}".format(i)).setItemText(0, val[0])     
                        getattr(self, "comboBox_{}".format(i)).setCurrentIndex(0)    

                    for i in range(1, 2):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "spinBox_{}".format(i)).setValue(int(val[0])) 
                    
                    for i in range(1, 2):
                        val = str(Info.readline()).split('\n')
                        getattr(self, "textEdit_{}".format(i)).setText(val[0])
                    
                    val = str(Info.readline()).split('\n')
                    Row = int(val[0])

                    self.table.setRowCount(Row)

                    for i in range(0, Row):
                        for j in range(0, 7):
                            val = str(Info.readline()).split('\n')
                            item = QTableWidgetItem(val[0])
                            self.table.setItem(i, j, item)
                   
        except Exception as e:
            print(e)

    def GuardarInfo(self):

        Info = self.GuardarFile()

        if Info == True:
            mesa="Correcto!"
            msg = QMessageBox()
            msg.setStyleSheet("""background-color: #12abc4;
                                    color: white;            
            """)
            msg.setIcon(QMessageBox.Information)
            msg.setText('Archivo guardado satisfactoriamente')
            msg.setInformativeText(mesa)
            msg.setWindowTitle("METROIL S.A.C.")
            msg.exec_()
        else:
            mesa="Error!"
            msg = QMessageBox()
            msg.setStyleSheet("""background-color: #12abc4;
                                    color: white;            
            """)
            msg.setIcon(QMessageBox.Critical)
            msg.setText('Colocar código del manómetro')
            msg.setInformativeText(mesa)
            msg.setWindowTitle("METROIL S.A.C.")
            msg.exec_()

    def GuardarFile(self):
        try:
            Ano = str(datetime.date.today()).split("-")
            Ano = Ano[0]            
            
            nom1 = "Manometros"
            nom2 = self.lineEdit_2.text()
            nom3 = Ano

            if (nom1 != "" and nom2 != "" and nom3 != ""):
                self.NameG = nom1+"-"+nom2+"-"+nom3+".cmil"
                Info = open(self.ruta + "\\Manometros\\Archivos Files\\" + self.NameG, 'w',encoding='utf-8')

                for i in range(2, 9):
                    Info.write(str(getattr(self, "lineEdit_{}".format(i)).text()) + '\n')
                
                for i in range(1, 4):
                    Info.write(str(getattr(self, "comboBox_{}".format(i)).currentText()) + '\n')
                
                for i in range(1, 2):
                    Info.write(str(getattr(self, "spinBox_{}".format(i)).value()) + '\n')
                
                for i in range(1, 2):
                    Info.write(str(getattr(self, "textEdit_{}".format(i)).toPlainText()) + '\n')
                
                Row = self.table.rowCount()

                Info.write(str(Row)+ '\n')

                rowdata = []
                for row in range(self.table.rowCount()):                        
                    for column in range(self.table.columnCount()):
                        item = self.table.item(row, column)                        
                        if item is not None:
                            rowdata.append(item.text())
                        else:
                            rowdata.append('')
                
                for i in range(0, len(rowdata)):
                    Info.write(str(rowdata[i])+'\n')
                
                Reenviar = True

            else:
                Reenviar = False

            return Reenviar

        except Exception as e:
            print(e)
    
    def Information(self):
        Ui_Informacion().exec_()

    def DeleteFile(self):
        Row = self.table.rowCount()
        self.table.setRowCount(Row - 1)
    
    def Refresh(self):
        self.hide()
        self.ui = Ui_Main_4()
        self.ui.show()

    def Regresar(self):
        self.close()
        self.ui = Start()
        self.ui.show()

    def AddFila(self):
        Row = self.table.rowCount()

        self.table.setRowCount(Row + 1)        

    def AgrandarMenuLateral(self):
        if True:
            width = self.leftMenuContenedor.width()
            norm = 65
            if width == 65:
                exten = 200
            else:
                exten = norm

            self.ani = QPropertyAnimation(self.leftMenuContenedor, b'minimumWidth')
            self.ani.setDuration(300)
            self.ani.setStartValue(width)
            self.ani.setEndValue(exten)
            self.ani.setEasingCurve(QtCore.QEasingCurve.InOutQuart)
            self.ani.start()

class Ui_Informacion(QDialog):
    def __init__(self):
        super(Ui_Informacion, self).__init__(None)
                
        uic.loadUi("Informacion.ui", self)
        self.setWindowIcon(QIcon('0.ico'))

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)    
    app.setWindowIcon(QtGui.QIcon('0.ico'))
    window = Start()
    #window = Ui_Main_2()    
    window.show()
    sys.exit(app.exec_())   