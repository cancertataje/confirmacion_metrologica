import numpy as np

def Deci2(num, decim):
    num = round(num, decim)
    if (num !=0):
        if (num != int(num)):
            numS = np.format_float_positional(num, trim='-') #--- Esta parte ha sido agrega el 2/12/2022  (Conserva su notación decimal y evita la científica)
            Cant = numS.split('.')
            
            Cant = len(Cant[1])

            if (Cant < decim):
                num = round(num, decim)
                num = np.format_float_positional(num, trim='-')

                for i in range(0, int(decim-Cant)):
                    num = num + "0"

            
                num = num.replace('.',',')

            elif (Cant == decim):
                num = round(num, decim)
                num = np.format_float_positional(num, trim='-')

                num = num.replace('.',',')

            elif (Cant > decim):
                num = round(num, decim)
                if (num != 0):
                    num = np.format_float_positional(num, trim='-')

                    num = num.replace('.',',')
                else:
                    num = str(num)
                    num = num.replace('.',',')

                    for i in range(0, decim-1):
                        num = num + "0"

        else:
            if(decim != 0):
                num = str(float(num))
                num = num.replace('.',',')
                for i in range(0, decim-1):
                    num = num + "0"
            else:
                num = int(round(num, 0))
                num = str(num)
            

    else:
        if(decim != 0):
            num = "0.0"
            num = str(num)
            num = num.replace('.',',')

            for i in range(0, decim-1):
                num = num + "0"
        else:
            num = "0"
    
    MilNum = num.split(',')
    
    if (len(MilNum) > 1):
        Millar = MilNum[0]

        if (len(Millar) == 4):
            num = Millar[0]+" "+Millar[1]+Millar[2]+Millar[3]+","+MilNum[1]
        elif (len(Millar) == 5):
            num = Millar[0]+Millar[1]+" "+Millar[2]+Millar[3]+Millar[4]+","+MilNum[1]
        elif (len(Millar) == 6):
            num = Millar[0]+Millar[1]+Millar[2]+" "+Millar[3]+Millar[4]+Millar[5]+","+MilNum[1]
        elif (len(Millar) == 7):
            num = Millar[0]+" "+Millar[1]+Millar[2]+Millar[3]+" "+Millar[4]+Millar[5]+Millar[6]+","+MilNum[1]
    
    else:
        Millar = num
        if (len(Millar) == 4):
            num = Millar[0]+" "+Millar[1]+Millar[2]+Millar[3]
        elif (len(Millar) == 5):
            num = Millar[0]+Millar[1]+" "+Millar[2]+Millar[3]+Millar[4]
        elif (len(Millar) == 6):
            num = Millar[0]+Millar[1]+Millar[2]+" "+Millar[3]+Millar[4]+Millar[5]
        elif (len(Millar) == 7):
            num = Millar[0]+" "+Millar[1]+Millar[2]+Millar[3]+" "+Millar[4]+Millar[5]+Millar[6]

    return num
